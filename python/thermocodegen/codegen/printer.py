# -*- coding: utf-8 -*-
"""Module for printing SymPy expressions"""

import sympy as sym
import re
from sympy.printing.cxxcode import CXX11CodePrinter
from sympy.printing.precedence import PRECEDENCE

class SubCodePrinter(CXX11CodePrinter):
    """
    Subclassing CXX11CodePrinter for customizing aspects of the C++ generation.
    """
    def _print_Pow(self, expr):
        if expr.exp.is_integer and expr.exp > 0 and expr.exp <= 4:
            """
            Overwrite std::pow where exponent is <= 4 (for speed) and
            """
            result = ')*('.join([self._print(expr.base) for i in range(expr.exp)])
            return '((' + result + '))'
        else:
            """
            Overwrite std::pow as pow so that it is compatible with the <math.h> library.
            """
            result = 'pow(' + self._print(expr.base) + ', ' + self._print(expr.exp) + ')'
            return result

    def _print_Function(self, expr):
        """
        Catches special code printing for affinity function A(T,P,C,j) to _A[j] else passes through
        """
        # Function name
        func_name = expr.func.__name__

        if (func_name == 'A'):
            j = str(expr.args[-1])
            return '_A[{}]'.format(j)
        else:
            return super()._print_Function(expr)


    def _print_Derivative(self, expr):
        """
        Modifies code printer when taking derivatives
        """
        # Function type
        func_name = expr.args[0].func.__name__

        # Replace 'mu' with 'G'
        if(func_name == 'mu'):
            func_name = 'G'

        # If function type if 'A' or 'rho' then use that specific printer
        #FIXME: rho not used anymore
        if func_name in ['A', 'rho']:
            result = self._print_external_derivative(expr,func_name)
        # Otherwise assume that this is an endmember property
        else:
            result = self._print_endmember_derivative(expr,func_name)

        return result

    def _print_MatrixElement(self, expr):
        """
        Overwrite Matrix printing
        """
        _var = self.parenthesize(expr.parent, PRECEDENCE["Atom"],strict=True)
        if _var == 'C':
            # Print concentration matrix element C[i,j] as C[i][j]
            return "C[{0}][{1}]".format(expr.i,expr.j)
        else:
            # Otherwise unroll matrix as a vector
            return "{0}[{1}]".format(_var, expr.j + expr.i*expr.parent.shape[1])

    def _print_endmember_derivative(self,expr,func_type):
        """
        Derivatives of endmember properties are replaced by their
        corresponding C++ pointers.
        """
        # Number of derivatives and function index
        number_of_derivatives = expr.derivative_count
        function_string_index = int(str(expr.args[0].func).split("_")[1])

        # Printer derivatives
        result = ''
        if number_of_derivatives == 1:
            derivative_string = sym.srepr(expr.args[1]).split("'")[1]
            result = '(*_endmembers[' + str(function_string_index) + ']).d{}d'.format(func_type) + derivative_string + '(T, P)'

        elif number_of_derivatives == 2:
            derivative_string_1 = sym.srepr(expr.variables[0]).split("'")[1]
            derivative_string_2 = sym.srepr(expr.variables[1]).split("'")[1]
            if derivative_string_1 == 'T' and derivative_string_2 == 'T':
                result = '(*_endmembers[' + str(function_string_index) + ']).d2{}dT2(T, P)'.format(func_type)
            elif derivative_string_1 == 'P' and derivative_string_2 == 'T':
                result = '(*_endmembers[' + str(function_string_index) + ']).d2{}dTdP(T, P)'.format(func_type)
            elif derivative_string_1 == 'P' and derivative_string_2 == 'P':
                result = '(*_endmembers[' + str(function_string_index) + ']).d2{}dP2(T, P)'.format(func_type)

        elif number_of_derivatives == 3:
            derivative_string_1 = sym.srepr(expr.variables[0]).split("'")[1]
            derivative_string_2 = sym.srepr(expr.variables[1]).split("'")[1]
            derivative_string_3 = sym.srepr(expr.variables[2]).split("'")[1]
            if derivative_string_1 == 'T' and derivative_string_2 == 'T' and derivative_string_3 == 'T':
                result = '(*_endmembers[' + str(function_string_index) + ']).d3{}dT3(T, P)'.format(func_type)
            elif derivative_string_1 == 'P' and derivative_string_2 == 'T' and derivative_string_3 == 'T':
                result = '(*_endmembers[' + str(function_string_index) + ']).d3{}dT2dP(T, P)'.format(func_type)
            elif derivative_string_1 == 'P' and derivative_string_2 == 'P' and derivative_string_3 == 'T':
                result = '(*_endmembers[' + str(function_string_index) + ']).d3{}dTdP2(T, P)'.format(func_type)
            elif derivative_string_1 == 'P' and derivative_string_2 == 'P' and derivative_string_3 == 'P':
                result = '(*_endmembers[' + str(function_string_index) + ']).d3{}dP3(T, P)'.format(func_type)

        return result

    def _print_external_derivative(self,expr,func_type):
        """
        Print C++ function string for derivatives of the external functions.
        """
        if func_type == 'A':
            j = str(expr.args[0].args[-1])
        # Variable we are differentiating w.r.t
        diff_var = sym.srepr(expr.args[1]).split("'")[1]

        if diff_var == 'C':
            # Extract indices of concentration matrix
            i_str = (sym.srepr(expr.args[1]).split(','))[-3]
            k_str = (sym.srepr(expr.args[1]).split(','))[-2]

            k = re.search('\(([^)]+)', k_str).group(1)
            i = re.search('\(([^)]+)', i_str).group(1)

            # Function arguments
            args = str(expr.args[0])[1:-1] + ', {i}, {k})'.format(i=i,k=k)
            #result = 'd{}j_dCik'.format(func_type) + args
            result = '_dA_dC[{}][{}][{}]'.format(j,i,k)

        else:
            func = str(expr.args[0])
            #result = 'd{}_d{}'.format(func_type,diff_var) + func[func.index('('):]
            result = '_d{}_d{}[{}]'.format(func_type,diff_var,j)

        return result

    def _print_Heaviside(self,expr):
        """Modifies Code Printing to replace Heaviside Functions with C++ ternary operators
                H(x) = ( x >= 0. ? 1. : 0.)
        """
        result = '({} >= 0. ? 1. : 0.)'.format(self._print(expr.args[0]))
        return result

    def _print_DiracDelta(self,expr):
        """Modifies Code Printing to replace Dirac Delta Function  with zeros and warnings
                For our models we only need first derivatives anyway
        """
        number_of_derivatives = len(expr.args) - 1
        if number_of_derivatives == 0:
            ## FIXME:  This is the correct thing for a delta function but probably impractical
            #result = '({} == 0. ? std::numeric_limits<double>::infinity() : 0.)'.format(self._print(expr.args[0]))
            result = '0'
            print('Warning: {} replaced with 0 by codeprinter'.format(expr))
        else:
            result = '0'
            print('Warning: {} replaced with 0 by codeprinter'.format(expr))
        return result

    def _print_Min(self, expr):
        """ redefine Min printer to replace pow"""
        from sympy import Min
        if len(expr.args) == 1:
            return self._print(expr.args[0])
        ## FIXME: check...this looks like is an old python 2 style format statement
        #return "%smin(%s, %s)" % (self._ns, self._print(expr.args[0]), self._print(Min(*expr.args[1:])))
        return '{}min({}, {})'.format(self._ns, self._print(expr.args[0]), self._print(Min(*expr.args[1:])))

