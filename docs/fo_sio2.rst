The Forsterite-|Si| system
=================================================

A more complex  system  than the Forsterite-Fayalite melting loop is the
Forsterite-|Si| binary which includes both peritectic and eutectic melting and
provides a tractable but important  analog system for mantle melting.
This example reproduces the custom Fo-|Si| models explored in depth by Lucy Tweed
in her thesis [Tweed_2021]_.

.. image:: ./images/Fo-sio2-phase_diagram_1bar.png
  :width: 45%
.. image:: ./images/Fo-sio2-phase_diagram_10kbar.png
  :width: 45%

*Phase diagrams at 1 bar and 10 kbar in the* Fo-|Si| *system demonstrating the transition
from peritectic melting at low pressures to Eutectic melting at higher pressures.*

System Description
++++++++++++++++++++

.. some useful compositional macros

.. |Si| replace:: SiO\ :sub:`2`
.. |Si2| replace:: Si\ :sub:`2`\ O\ :sub:`4`
.. |Fo| replace:: Mg\ :sub:`2`\ SiO\ :sub:`4`
.. |En| replace:: Mg\ SiO\ :sub:`3`
.. |Fo_ol| replace:: |Fo|\ :sup:`ol`
.. |Fo_lq| replace:: |Fo|\ :sup:`lq`
.. |Si_lq| replace:: |Si|\ :sup:`lq`
.. |Si2_lq| replace:: |Si2|\ :sup:`lq`
.. |En_opx| replace:: |En|\ :sup:`opx`
.. |Si_qz| replace:: |Si|\ :sup:`qz`


*Thermodynamic Database*
_________________________

This system is described by a custom **Thermodynamic Database** consisting of
four phases, constructed from five endmembers.

The first phase is silicate **Liquid** constructed as an asymmetric regular solution between
two endmembers

* Forsterite (|Fo_lq|)
* Qz4 (|Si2_lq|)

where |Fo_lq| is the forsteritic xMelts model and Qz4 is an endmember constructed from
two mols of an xMelts |Si_lq| model.

The next three phases are pure phases each constructed from a single endmember

* Olivine (endmember: Forsterite, |Fo_ol|)
* Opx (endmember: Orthoenstatite, |En_opx|)
* pQz (endmember: polymorphic |Si|, |Si_qz|)

where pQz is constructed from a single polymorphic endmember (see below). All of the mineral
endmembers are from [Berman_1988]_.

*Reaction Kinetics*
___________________

Given this  system,  there are three reactions involved in melting and
crystallization of Olivine, Opx, and pQz


* **Olivine Melting**:

    |Fo_ol| :math:`\rightarrow` |Fo_lq|

* **Enstatite Melting**:

    |En_opx| :math:`\rightarrow\frac{1}{2}` |Fo_lq| + :math:`\frac{1}{4}` |Si2_lq|

* **Quartz Melting**:

    |Si_qz| :math:`\rightarrow\frac{1}{2}`  |Si2_lq|


All thermodynamic models for endmembers, phases, and kinetic reactions are
described in a series of jupyter notebooks in the directory
:code:`Systems/fo_sio2/notebooks` and relevant sub-directories.  All paths and commands described below are
assumed to be relative to :code:`Systems/fo_sio2` (see :doc:`quickstart` for
specific instructions on extracting the ``Systems`` directory from the containers).

Modeling workflow
+++++++++++++++++

This demo will illustrate the full workflow of a ThermoCodegen project.

1. Describe the thermodynamic models for endmembers in jupyter notebooks using ``coder`` models and store them  as ``*.emml`` files
2. Describe thermodynamic models for phases built on endmembers in jupyter notebooks and store them as ``*.phml`` files
3. Generate source code from model description files using the script ``tcg_builddb`` to construct a custom **Thermodynamic Database** of endmembers and phases
4. Describe reactions between endmembers in phasesin jupyter notebooks and store them as ``*.rxml`` files
5. Generate a compiled C++ library with python bindings for all code generated objects
6. Test the compiled libraries against benchmark solutions
7. Import the python bindings into jupyter notebooks to perform calculations (like calculating the phase diagrams shown above)

Quick build for the Fo-|Si| system
___________________________________________________

Steps 1--6 can be run using the shell script ``build_system.sh`` in the
directory ``Systems/fo_sio2``.

.. code-block:: bash

  bash ./build_system.sh

Details of ``build_system.sh``
++++++++++++++++++++++++++++++++++
This section describes in detail the various phases of the build process


Jupyter notebooks describing endmembers and phases
___________________________________________________

**Endmembers**: There are four jupyter notebooks that describe, in detail,  thermodynamic models
for endmembers. In the directory ``notebooks/endmembers``

* **Solid Endmembers**

  *  :doc:`Generate_berman_endmembers.ipynb  <notebooks/fo_sio2/Generate_berman_endmembers>` : calculates pure forsterite and orthoenstatite endmembers using the model of [Berman_1988]_
  *  :doc:`Generate-Polymorphic-Quartz.ipynb <notebooks/fo_sio2/Generate-Polymorphic-Quartz>` : calculate a single polymorphic solid |Si| model as

    .. math::

	      G_{pQz}(T,P)=\min[G_{\beta-Qz}(T,P),G_{\beta-Crs}(T,P)]

    where :math:`\beta-Qz` and :math:`\beta-Crs` are [Berman_1988]_ endmembers for B-quartz and beta-Cristobalite


* **Liquid Endmembers**

  *  :doc:`Generate_xmelts_endmembers.ipynb <notebooks/fo_sio2/Generate_xmelts_endmembers>`: calculate Fo and |Si2| liquid endmembers using the xMelts model of Ghiorso
  *  :doc:`Generate_Qz4-liquid.ipynb <notebooks/fo_sio2/Generate_Qz4-liquid>`: calculate |Si2_lq| endmember from 2 mols of |Si_lq| xMelts model


**Phases**: There are two jupyter notebooks that describe, in detail, thermodynamic models
for phases.  In the directory ``notebooks/phases``

* :doc:`Generate_pure_phases.ipynb <notebooks/fo_sio2/Generate_pure_phases>`: describe thermodynamic models for the 3 pure phases, Olivine, Opx, and pQz built on the solid endmembers above.
* :doc:`Generate_asymmetric_binary_fosi_solution.ipynb <notebooks/fo_sio2/Generate_asymmetric_binary_fosi_solution>`: describes the thermodynamic model for the silicate liquid as a the two-component asymmetric regular solution used [Tweed_2021]_ using the :class:`coder.SimpleSolnModel`

These notebooks can be run manually or executed as scripts using

.. code-block:: bash

  # generate spudfiles and build  thermodynamic database
  cd notebooks
  for d in endmembers phases
  do
    files=$d/*.ipynb
    for file in $files
    do
      jupyter nbconvert --to notebook --execute $file
    done
  done


The endmember notebooks will generate a set of endmember ``.emml`` files and put them in the directory ``endmembers``.
The phase notebooks will generate a set of phase ``.phml`` files and put them in the directory ``phases``.

Viewing ThermoCodegen xml files with ``diamond``
******************************************************

any of the spud xml files can be viewed directly using the ``diamond`` gui
available in the containers.  For example, running

.. code-block:: bash

  diamond phases/Olivine.phml

from within a singularity container (or a docker container with appropriate X11 configuration) in the ``Systems/fo_sio2`` subdirectory
should pop open a ``diamond`` window that includes all the parameters and sympy
expressions required to generate code for the Olivine model.


.. .. image:: images/Diamond-Olivine-screenshot.png
  :width: 90%
  :align: center


.. note::

	It is possible to work with the xml files directly through the ``diamond``
	interface but it is usually easier to generate them from jupyter notebooks
	using the python :ref:`py_code_generation`  then writing ``.to_xml``.

Build the thermodynamic database using :doc:`tcg_builddb`
_________________________________________________________

The primary thermocodegen script :doc:`tcg_builddb`  reads the model description  files and
autogenerates source code from the xml files and generates a compressed **Thermodynamic Database** file.

Usage: from within the ``Systems/fo_sio2`` directory

.. code-block:: shell

  tcg_builddb --just_src  -l fo_sio2_db -zi database

which will generate the file ``database/fo_sio2_db.tar.gz``

Other available commands can be found with ``tcg_builddb -h`` or see :doc:`tcg_builddb`.
This script should be run in the directory above the directories containing the
relevant ``.emml`` and ``.phml`` files.


Given a directory or subdirectory containing a set of ``.emml`` and
``.phml`` files, we can inspect and validate those files (i.e. check that the
phases are completely described by the endmembers). Running

.. code-block:: bash

  tcg_builddb -evp

from within the ``Systems/fo_sio2`` directory should produce the output

.. code-block:: bash

    **** Available Endmembers ***
             Name               Formula                  File
             B_Quartz_berman       SI(1)O(2)           B_Quartz_berman.emml
           Forsterite_xmelts  MG(2)SI(1)O(4)         Forsterite_xmelts.emml
              Quartz4_xmelts       SI(2)O(4)            Quartz4_xmelts.emml
       Orthoenstatite_berman  MG(1)SI(1)O(3)     Orthoenstatite_berman.emml
           Forsterite_berman  MG(2)SI(1)O(4)         Forsterite_berman.emml
     Silica_polymorph_berman       SI(1)O(2)   Silica_polymorph_berman.emml
              Quartz4_liquid       SI(2)O(4)            Quartz4_liquid.emml
    Beta_Cristobalite_berman       SI(1)O(2)  Beta_Cristobalite_berman.emml


    **** Available Phases ***
    Abbrev       Name                File
    Lq   Liquid                        Liquid.phml
    Qz   Quartz                        Quartz.phml
    Opx  Orthopyroxene          Orthopyroxene.phml
    Ol   Olivine                      Olivine.phml
    Crs  Cristobalite            Cristobalite.phml
    pQz  Silica_polymorph    Silica_polymorph.phml


    Liquid
     OK  Quartz4_liquid
     OK  Forsterite_xmelts
    Quartz
     OK  B_Quartz_berman
    Orthopyroxene
     OK  Orthoenstatite_berman
    Olivine
     OK  Forsterite_berman
    Cristobalite
     OK  Beta_Cristobalite_berman
    Silica_polymorph
     OK  Silica_polymorph_berman




Jupyter notebooks describing reactions
___________________________________________________

The jupyter notebook :doc:`Generate_fo_sio2_poly_linear_rxns.ipynb <notebooks/fo_sio2/Generate_fo_sio2_poly_linear_rxns>`
creates an ``.rxml`` file describing the three melt mediated reactions described above using phases and endmembers
described in the local thermodynamic database file ``database/fo_sio2_db.tar.gz``.

it is run using

.. code-block:: shell

  jupyter nbconvert --to notebook --execute notebooks/reactions/Generate_fo_sio2_poly_linear_rxns.ipynb


this script will generate a file ``reactions/fo_sio2_poly_linear_rxns.rxml``.


Build the reaction object using :doc:`tcg_buildrx`
___________________________________________________

.. code-block:: bash

  cd reactions
  tcg_buildrx fo_sio2_poly_linear_rxns.rxml -i

uses CMake to compile a C++ library and its python bindings  from the ``.rxml``
file that also includes all of the endmember and phase objects.

.. note::

	:doc:`tcg_buildrx` assumes that the first argument is an ``.rxml`` file.  For
	more options see :doc:`tcg_buildrx`.


Testing the models
__________________

At this point there should be a subdirectory in the ``reactions`` directory called
``fo_sio2_poly_linear_rxns`` which contains

* ``fo_sio2_poly_linear_rxns.module``
*	``fo_sio2_poly_linear_rxns.senv``
* ``include``
* ``lib``
* ``src``

If you are using environment modules, you can set your environment using

.. code-block:: bash

  module load ./reactions/fo_sio2_poly_linear_rxns/fo_sio2_poly_linear_rxns.module


Alternatively if you are using singularity,  you can leave and restart the shell with

.. code-block:: bash

  singularity shell --env-file <path to>/fo_sio2_poly_linear_rxns.senv <path to>/thermocodegen_tf-focal.sif

The file ``fo_sio2_poly_linear_rxns.senv`` is a singularity environment file generated during
generation of the reaction object which adds the reaction  environment variables to the
container.

Running ``pytest``
******************

You can now test the python bindings with

.. code-block:: bash

  cd tests
  pytest --disable-warnings test*

which if successful should return something similar to

.. code-block:: bash

  ============================= test session starts ==============================
  platform darwin -- Python 3.7.5, pytest-4.0.0, py-1.7.0, pluggy-0.8.0
  rootdir: /Users/mspieg/Repos/gitlab/ThermoCodegen/examples/Systems/fo_sio2/tests, inifile:
  collected 514 items

  test-v0.6.7-2021-06-05_13:47:43/test_endmembers_05-Jun-2021_13:48:10.py . [  0%]
  ........................................................................ [ 14%]
  ........................................................................ [ 28%]
  .......................................................................  [ 42%]
  test-v0.6.7-2021-06-05_13:47:43/test_phases_05-Jun-2021_13:51:46.py .... [ 42%]
  ........................................................................ [ 56%]
  ........................................................................ [ 70%]
  ........................................................................ [ 84%]
  ............................................                             [ 93%]
  test-v0.6.7-2021-06-05_13:47:43/test_rxns_05-Jun-2021_13:52:25.py ...... [ 94%]
  ............................                                             [100%]

  ==================== 514 passed in 1.49 seconds ===================


Generating test files
**********************

The test files included in this example were generated with the jupyter notebook
:doc:`Generate_tests.ipynb <notebooks/fo_sio2/Generate_tests>`  in the ``notebooks/tests`` directory.  This notebook uses the
:doc:`Tester <testing>` class imported from ``thermocodegen.testing`` which
takes in a ThermoCodegen reaction or database object and tests all of its
methods against known input values.  Output of ``Tester``  includes
``pandas.DataFrames`` of output values as well as ``pytest`` test files.


Using ThermoCodegen objects
+++++++++++++++++++++++++++

Once the ThermoCodegen objects are compiled, their python bindings can be
imported directly into any python project or Juypter Notebook and be used to
explore the system and generate models or figures.  As an example, the notebook
:doc:`phase_diagram.ipynb <notebooks/fo_sio2/phase_diagram>` in the directory
``notebooks/applications`` provides code for generating the isobaric
phase-diagrams shown above, and :doc:`Isentropic_columns.ipynb <notebooks/fo_sio2/Isentropic_columns>`
demonstrates how to calculate steady-state 1-D isentropic upwelling columns for batch melting problems.

Jupyter Notebooks
+++++++++++++++++

Endmembers
__________

.. toctree::
  :maxdepth: 1

  Generate Berman endmembers <notebooks/fo_sio2/Generate_berman_endmembers>
  Generate xMelts endmembers <notebooks/fo_sio2/Generate_xmelts_endmembers>
  Generate polymorphic quartz <notebooks/fo_sio2/Generate-Polymorphic-Quartz>
  Generate liquid <notebooks/fo_sio2/Generate_Qz4-liquid>

Phases
______

.. toctree::
  :maxdepth: 1

  Generate pure phases <notebooks/fo_sio2/Generate_pure_phases>
  Generate asymmetric binary solution phases <notebooks/fo_sio2/Generate_asymmetric_binary_fosi_solution>


Reactions
_________

.. toctree::
  :maxdepth: 1

  Generate reactions <notebooks/fo_sio2/Generate_fo_sio2_poly_linear_rxns>

Tests and Applications
______________________

.. toctree::
  :maxdepth: 1

  Generate tests <notebooks/fo_sio2/Generate_tests>
  Phase diagram <notebooks/fo_sio2/phase_diagram>
  1-D isentropic melting columns <notebooks/fo_sio2/Isentropic_columns>

