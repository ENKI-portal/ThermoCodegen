{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## This notebook generates a rxml file for Solid State reactions in the MgFeSiO4 Stixrude solution phases"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from thermocodegen.codegen import reaction\n",
    "import sympy as sym\n",
    "import os\n",
    "from glob import glob\n",
    "sym.init_printing()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Set a reference string for this Notebook"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "reference = 'MgFeSiO2_stixrude/notebooks/Generate_reactions.ipynb'"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "List of phases present in this set of reactions"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "phase_names = ['Olivine', 'Wadsleyite', 'Ringwoodite']"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Path to the thermodynamic database tarball file that this set of reactions are built with\n",
    "\n",
    "Here we use a relative path to a local  thermodynamic database file.  This can lead to issues if the final rxml file is moved so an absolute path to a local file is safer, however, it also makes the rxml files less easy to share among different users."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "db = '../database/MgFeSiO4_Stixrude.tar.gz'\n",
    "print(db)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Alternatively, we could use a remote thermodynamic database, for example the one available as a DOI from zenodo \n",
    "\n",
    "[![DOI](https://zenodo.org/badge/DOI/10.5281/zenodo.7976277.svg)](https://doi.org/10.5281/zenodo.7976277)\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "#db = 'https://zenodo.org/record/7976277/files/MgFeSiO4_Stixrude.tar.gz'"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Instantiate a Reaction object. This is initialized using the 'from_database' class method. Initializing in this way requires the total number of reaction (total_reactions), a list of phase names (phase_names) and a path to a thermocodegen generated thermodynamic database."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "rxn = reaction.Reaction.from_database(total_reactions=4, phase_names=phase_names, database=db)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The 'model_dict' attribute of this object contains the current state of the information that we have given and what has been extracted from the thermodynamic database 'fo_fa_coder_db.tar.gz'"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "rxn.model_dict"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Set some utility indices for referencing phases and endmembers (this should be automated somehow)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "iOl = 0\n",
    "iWa = 1\n",
    "iRi = 2\n",
    "kMg = 0\n",
    "kFe = 1"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Define the reactions in this system, and then write SymPy that describes the reactions rates"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "First we pull out the SymPy symbols for the independent variables (temperature T, pressure P, concentration C and phase fraction Phi). Concentration C is a sym.MatrixSymbol of dimension N (number of phases) by Kmax (maximum number of endmembers). Phase fraction Phi is a sym.MatrixSymbol of dimension N (number of phases) by 1 (i.e., a vector)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Variables\n",
    "T = rxn.T\n",
    "P = rxn.P\n",
    "C = rxn.C\n",
    "Phi = rxn.Phi"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now we pull out the special symbol reserved for the affinity, A. This is a sym.MatrixSymbol of dimension J (number of reactions) by 1."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Affinity\n",
    "A = rxn.A"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now we define the parameters used in the reaction rate expressions. These should be sym.Symbol objects. We also need to create lists that contain the parameter names as strings, their units (in string form) and the corresponding SymPy symbols. Note that these need to be ordered correctly."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Parameters\n",
    "T0 = sym.Symbol('T0')\n",
    "R = sym.Symbol('R')\n",
    "\n",
    "params = ['T0','R']\n",
    "units = ['K','J/(mol K)']\n",
    "symparam = [T0, R]"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Forsterite to Wadleyite Reaction"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Set up the forsterite to wadsleyite reaction. This requires a list of tuples for both the reactants and products. Each tuple consists of strings of \n",
    "\n",
    "(name,phase,endmember)\n",
    "\n",
    "where \n",
    "    **name**:  is an arbitrary name describing the reactant endmenber,\n",
    "    **phase**: is the phase containing the endmember\n",
    "    **endmember**: the name of the endmember that is reacting in that phase\n",
    "    \n",
    "The phase and endmember names should be consistent with whatever they are called in the thermodynamic database."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# reaction 0\n",
    "j = 0\n",
    "\n",
    "# Reactants\n",
    "Fo_Ol = ('Fo_Ol','Olivine','Forsterite_stixrude')\n",
    "\n",
    "# Products\n",
    "MgWa_Wa = ('MgWa_Wa','Wadsleyite','MgWadsleyite_stixrude')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now we write down the SymPy expression for the reaction. This, along with the list of reactants, products, parameters, units and variable symbols are passed into the 'add_reaction_to_model' function. This function also requires a 'name' field, which should be consistent with the variable assigned to the SymPy expression; for example, here our expression is Fo_melting = ..., so our name is 'Fo_melting'."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "\n",
    "# SymPy expression for reaction rate\n",
    "r = sym.exp(-T0/T)\n",
    "rp = r\n",
    "rm = r\n",
    "\n",
    "S0p = Phi[iOl]\n",
    "S0m = Phi[iWa]\n",
    "Fo_MgWa = sym.Piecewise((rp*S0p*A[j]/R/T,A[j]>=0),(rm*S0m*A[j]/R/T,A[j]<0),(0,True))\n",
    "Fo_MgWa"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "reactants = [ Fo_Ol]\n",
    "products = [ MgWa_Wa]\n",
    "rxn.add_reaction_to_model('Fo_MgWa', reactants, products, Fo_MgWa, list(zip(params,units,symparam)))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Repeat for Fe endmember"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# reaction 1\n",
    "j = 1\n",
    "\n",
    "# Reactants\n",
    "Fa_Ol = ('Fa_Ol','Olivine','Fayalite_stixrude')\n",
    "\n",
    "# Products\n",
    "FeWa_Wa = ('FeWa_Wa','Wadsleyite','FeWadsleyite_stixrude')"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# SymPy expression for reaction rate\n",
    "r = sym.exp(-T0/T)\n",
    "rp = r\n",
    "rm = r\n",
    "\n",
    "S0p = Phi[iOl]\n",
    "S0m = Phi[iWa]\n",
    "Fa_FeWa = sym.Piecewise((rp*S0p*A[j]/R/T,A[j]>=0),(rm*S0m*A[j]/R/T,A[j]<0),(0,True))\n",
    "Fa_FeWa"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "reactants = [ Fa_Ol]\n",
    "products = [ FeWa_Wa]\n",
    "rxn.add_reaction_to_model('Fa_FeWa', reactants, products, Fa_FeWa, list(zip(params,units,symparam)))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Do the same for the Wad to Ringwoodite reactions"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# reaction 2\n",
    "j = 2\n",
    "\n",
    "# Reactants\n",
    "MgWa_Wa = ('MgWa_Wa','Wadsleyite','MgWadsleyite_stixrude')\n",
    "\n",
    "# Products\n",
    "MgRi_Ri = ('MgRi_Ri','Ringwoodite','MgRingwoodite_stixrude')"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# SymPy expression for reaction rate\n",
    "r = sym.exp(-T0/T)\n",
    "rp = r\n",
    "rm = r\n",
    "\n",
    "S1p = Phi[iWa]\n",
    "S1m = Phi[iRi]\n",
    "MgWa_MgRi = sym.Piecewise((rp*S1p*A[j]/R/T,A[j]>=0),(rm*S1m*A[j]/R/T,A[j]<0),(0,True))\n",
    "MgWa_MgRi"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "reactants = [ MgWa_Wa ] \n",
    "products = [ MgRi_Ri ]\n",
    "rxn.add_reaction_to_model('MgWa_MgRi', reactants, products, MgWa_MgRi, list(zip(params,units,symparam)))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Repeat for Fe endmembers"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# reaction 3\n",
    "j = 3\n",
    "\n",
    "# Reactants\n",
    "FeWa_Wa = ('FeWa_Wa','Wadsleyite','FeWadsleyite_stixrude')\n",
    "\n",
    "# Products\n",
    "FeRi_Ri = ('FeRi_Ri','Ringwoodite','FeRingwoodite_stixrude')"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# SymPy expression for reaction rate\n",
    "r = sym.exp(-T0/T)\n",
    "rp = r\n",
    "rm = r\n",
    "\n",
    "S1p = Phi[iWa]\n",
    "S1m = Phi[iRi]\n",
    "FeWa_FeRi = sym.Piecewise((rp*S1p*A[j]/R/T,A[j]>=0),(rm*S1m*A[j]/R/T,A[j]<0),(0,True))\n",
    "FeWa_FeRi"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "reactants = [ FeWa_Wa ] \n",
    "products = [ FeRi_Ri ]\n",
    "rxn.add_reaction_to_model('FeWa_FeRi', reactants, products, MgWa_MgRi, list(zip(params,units,symparam)))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### The model_dict has now been updated to contain all of the information for these reactions"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "rxn.model_dict"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Return a dictionary of settable name value pairs"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "values_dict = rxn.get_values()\n",
    "values_dict"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Update some of these values...\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "values_dict.update(dict(name='MgFeSiO4_stixrude',\n",
    "                        reference=reference,\n",
    "                        T0=2000.,\n",
    "                        R=8.31442))\n",
    "values_dict"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "... and update them in the model_dict using the 'set_values' function"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "rxn.set_values(values_dict)\n",
    "rxn.model_dict"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Generate Spud XML files"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Set some directories"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "HOME_DIR = os.path.abspath(os.curdir)\n",
    "SPUD_DIR = HOME_DIR+'/../reactions'\n",
    "\n",
    "try:\n",
    "    os.mkdir(SPUD_DIR)\n",
    "except:\n",
    "    pass"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Dump the spud file"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "rxn.to_xml(path=SPUD_DIR)"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.9.5"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
