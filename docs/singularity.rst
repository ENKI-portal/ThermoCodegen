Running TCG with singularity
===================================

.. note::
   The following commands have been tested with singularity >= 3.7.

If you have singularity installed on your machine, the most current singularity container can be built
from a docker container using

.. code-block:: bash

   singularity pull docker://registry.gitlab.com/enki-portal/thermocodegen:tf-focal

which will create the Singularity Image File ``thermocodegen_tf-focal.sif`` which  also contains a full implementation
of `TerraFERMA  <https://terraferma.github.io>`_.

Singularity transparently handles both file ownership and X11 forwarding needed
to use the option file GUI ``diamond``.

To start singularity in interactive shell mode

.. code-block:: bash

  singularity shell thermocodegen_tf-focal.sif

which will launch a singularity shell  that acts to all intents
and purposes as your regular shell, however it gives access to
executables and files within the container.   If started correctly, it should
have changed your prompt to

.. code-block:: bash

  Singularity>

To run the tests, we also need to initialize environment-modules within the containers with

.. code-block:: bash

  Singularity> source /usr/share/modules/init/bash

.. note::

    Singularity will inherit environment variables from the host machine if they are not overwritten
    in the container.  This can sometimes cause conflicts, particularly if different versions of modules are
    used on the host and container.  There are multiple mechanisms for controlling the container environment variables
    and ThermoCodegen provides ``*.senv`` files for setting variables necessary to work with ThermoCodegen objects
    through the ``--env-file`` parameter which can be important in a cluster job-scheduling system (see below).
    For more information on Singularity and environment variables see
    the Singularity `documentation <https://docs.sylabs.io/guides/latest/user-guide/environment_and_metadata.html>`_.

Working with the examples
----------------------------------

A set of example thermodynamic systems are included in the container. To get
started, from within the singularity shell

.. code-block:: bash

  Singularity> mkdir examples
  Singularity> cp -r  $THERMOCODEGEN_HOME/share/thermocodegen/examples/Systems examples
  Singularity> cd examples/Systems
  Singularity> ls


which should show 4 different directories for the Example Systems

* ``fo_fa``: :doc:`fo_fa`.
* ``fo_sio2``: :doc:`fo_sio2`. A custom asymmetric regular solution from Tweed (2021).
* ``MgFeSiO4_Stixrude``: :doc:`Stixrude`: A selection of phases in the MgFeSiO4 system using the thermodynamic models of Stixrude and Lithgow-Bertelloni (2005, 2011).
* ``fo_h2o``: :doc:`fo_h20` using the Ghiorso SWIM Water model.


Quick check
------------
To check that the singularity installation is working, try

.. code-block:: bash

    Singularity> cd fo_fa
    Singularity> bash build_system.sh

This will generate  all the  thermodynamic model descriptions and source code and compile it into
a loadable python module and test it using ``pytest``.

A successful test will complete with

.. code-block:: bash

    ===================================================================== test session starts ======================================================================
    platform linux -- Python 3.8.10, pytest-4.6.9, py-1.11.0, pluggy-0.13.1
    rootdir: /burg/apam/users/mws6/test/examples/Systems/fo_fa/tests
    collected 274 items

    test-v0.6.9-2022-07-21_00:15:09/test_endmembers_21-Jul-2022_00:15:09.py ................................................................................ [ 29%]
    ............................                                                                                                                             [ 39%]
    test-v0.6.9-2022-07-21_00:15:09/test_phases_21-Jul-2022_00:15:10.py .................................................................................... [ 70%]
    ................................................                                                                                                         [ 87%]
    test-v0.6.9-2022-07-21_00:15:09/test_rxns_21-Jul-2022_00:15:11.py ..................................                                                     [100%]

    ================================================================== 274 passed in 1.33 seconds ==================================================================

Full details of the workflow described in ``build_system.sh`` are provided for each example in  :doc:`examples`.

Running Jupyter Lab
--------------------

To work interactively with any of the jupyter notebooks in the examples you will need to be able
to run ``jupyter-lab`` from within the container.  This section will provide brief instructions
for running ``jupyter-lab`` from a singularity container running on a local machine.

If not already running, start an interactive singularity shell

.. code-block:: bash

  singularity shell thermocodegen_tf-focal.sif

and initialize modules within singularity

.. code-block:: bash

  Singularity> source /usr/share/modules/init/bash

Load whatever appropriate ThermoCodegen modules are required within the notebook (assuming they have been previously built, e.g.
using ``build_systems.sh`` above), e.g.

.. code-block:: bash

  Singularity> cd <path to>/examples/Systems/fo_fa
  Singularity> module load ./reactions/fo_fa_binary/fo_fa_binary.module

Start ``jupyter-lab``

.. code-block:: bash

    Singularity> jupyter-lab

which should start a jupyter server and produce something like

.. code-block:: bash

    To access the server, open this file in a browser:
        file:///home/tfuser/.local/share/jupyter/runtime/jpserver-629-open.html
    Or copy and paste one of these URLs:
        http://18238b848dc0:8888/lab?token=5a8136d8efa7f969189ba5c971e42f8b40a4988bc73158f6
     or http://127.0.0.1:8888/lab?token=5a8136d8efa7f969189ba5c971e42f8b40a4988bc73158f6

Cut and paste the last line, e.g.

.. code-block:: bash

    http://127.0.0.1:8888/lab?token=5a8136d8efa7f969189ba5c971e42f8b40a4988bc73158f6

into a browser on your local machine and you should be good to go.

As a test, in jupyter, open and run a relevant notebook, e.g. ``notebooks/Examples_Fo_Fa_system.ipynb``, and compare the output those available in :doc:`examples` (e.g. :doc:`Examples_Fo_Fa_system.ipynb <notebooks/fo_fa/Examples_Fo_Fa_system>`).

Running Singularity on a remote machine or cluster
--------------------------------------------------

A common environment for singularity containers are on remote linux machines, and in particular, HPC clusters.
Getting jupyter to work correctly in an HPC cluster can be very
cluster dependent, but this section provides some guidance and  has  been tested on one of Columbia's clusters.
Your results may vary.

Start by logging into the remote machine through ssh e.g.

.. code-block:: bash

    ssh -Y <username>@<machine address>


Running an interactive session remotely
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. note::
    If this is a shared HPC cluster, you should probably start an interactive session using whatever scheduler is installed on the cluster, e.g. ``srun -i`` with `SLURM  <https://slurm.schedmd.com/documentation.html>`_.

Start an interactive shell

.. code-block:: bash

    singularity shell thermocodegen_tf-focal.sif

and initialize environment modules as usual

.. code-block:: bash

    Singularity> source /usr/share/modules/init/bash

Load whatever appropriate ThermoCodegen modules are required, e.g.

.. code-block:: bash

    Singularity> cd <path to>/examples/Systems/fo_fa
    Singularity> module load ./reactions/fo_fa_binary/fo_fa_binary.module

If you don't need to run an interactive jupyter lab session through your
browser, a remote session should now work the same as a local session and it is possible to execute
jupyter notebooks in batch mode using, e.g.

.. code-block:: bash

    Singularity> jupyter nbconvert --to notebook --execute notebooks/Examples_Fo_Fa_system.ipynb

Running ``jupyter-lab`` interactively however, adds a few extra challenges.  If you want to be 
able to examine or work with jupyter-notebooks on the cluster
you will need to set up jupyter and ssh to port forward to your local browser.

Start by setting a jupyter password

.. code-block:: bash

  Singularity> jupyter notebook password

It will request and verify a password.

.. note::

    You only need to set a password once per host but if you forget your previous password you can
    always set another. You will need to remember it for this session

Get the IP address of your host node

.. code-block:: bash

    Singularity> ip=`hostname -i`

Start ``jupyter-lab`` within the container

.. code-block:: bash

    Singularity> jupyter-lab --port=8888 --no-browser --ip=${ip}

which will produce a lot of output but the key lines will be something like

.. code-block:: bash

    Jupyter Notebook 6.4.12 is running at:
    http://10.197.16.10:8888/


**On your local machine** open a **new** terminal and create an ssh tunnel with port forwarding to listen to the port ``jupyter-lab`` is using (in this case 8888) on the cluster node

.. code-block:: bash

    ssh -L 8080:10.197.16.10:8888 <username>@<machine address>

Open you **local** browser and navigate to the URL

.. code-block:: bash

    http://localhost:8080/lab

where 8080 is the port that the ssh tunnel is using locally.

Jupyter lab should start up and prompt you for the jupyter password you
created earlier.  If successful you should see the standard ``jupyter-lab`` interface.

.. warning::
    For this to work properly the port mapping and the hostnames need to be correct.  In this example,  the option
    ``-L 8080:10.197.16.10:8888`` says map port 8888 on the remote host (10.197.16.10) to the local port 8080.  The remote
    and local ports can use different numbers but they will need to be consistent when starting ``jupyter-lab`` remotely and
    opening up a local browser.

As a test of this example, navigate to the fo_fa example and open and run the notebook ``notebooks/Examples_Fo_Fa_system.ipynb`` and compare the output
to :doc:`Examples_Fo_Fa_system.ipynb <notebooks/fo_fa/Examples_Fo_Fa_system>`

Running singularity in exec mode
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

For non-interactive sessions, such as those encountered in batch job submission scripts on clusters, it is useful
to invoke singularity in `exec` mode.  Here we illustrate this with the TerraFERMA example described in detail in :doc:`tf_tcg`
which uses docker to build a reactions object and then drive a TerraFERMA simulation.

We can also use singularity in exec mode to execute those tasks.  To generate the database, assuming a copy of the ThermoCodegen ``examples`` directory is available on the machine (either by copying it in an interactive singularity shell as demonstrated earlier or by cloning the ThermoCodegen repository)

.. code-block:: bash

    export SIF=<absolute path to>/thermocodegen_tf-focal.sif
    cd <path to>/examples/Systems/fo_sio2/terraferma/reactions
    singularity exec --cleanenv ${SIF} tcg_buildrx fo_sio2_poly_linear_rxns.rxml -i

which should build a reaction object from a remote thermodynamic database hosted at
`zenodo <https://zenodo.org/record/7976277>`_. This command will create the directory ``fo_sio2_poly_linear_rxns`` which
includes a singularity environment file ``fo_sio2_poly_linear_rxns.senv`` that contains the environment variables required
by the container to use the generated ThermoCodegen objects.

To then run the TerraFERMA simulation with proper environment variables, 

.. code-block:: bash

    cd ../1D-isentropic
    SENV_FILE=../reactions/fo_sio2_poly_linear_rxns/fo_sio2_poly_linear_rxns.senv
    singularity exec --cleanenv --env-file ${SENV_FILE} ${SIF} tfsimulationharness --test 1D-isentropic.shml

which should complete successfully with

.. code-block:: bash

    Maximum Errors
    Da = 100.:	 7.2995498535e-11
    Da = 1000.:	 4.7611237175e-10
    Da = 2000.:	 4.0307826632e-07
    all errors <= 1e-06
    1D-isentropic.shml: success.
    1D-isentropic.shml: Running finish_time:
    1D-isentropic.shml: success.
    1D-isentropic.shml: Running err_file:
    1D-isentropic.shml: success.
    1D-isentropic.shml: PPP
    Passes:   3
    Failures: 0

See :doc:`tf_tcg` for more details

.. note::

    The ``--cleanenv`` option passed to ``singularity exec`` ensures that no environment variables from the host machine are passed
    to the container with the exception of those in the ``--env-file``.  This may be problematic in an interactive session
    where some host environment variables are needed.

All of these commands are readily incorporated into shell scripts or job submission scripts.





