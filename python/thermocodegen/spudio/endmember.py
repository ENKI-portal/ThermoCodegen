#!/usr/bin/env python3

"""
   endmember:

    collection of python routines for reading and writing Spud xml files
    to and from nested python dictionaries for use in general code-generation
    schemes
"""

from collections import Counter
from sympy.core.compatibility import exec_
# import libspud for reading and writing SPuD xml files
import libspud
import textwrap
from ..coder import coder




# import sympy for symbolic preprocessing
import sympy as sym

try:
    from .optionparsers import *
except ModuleNotFoundError:
    from optionparsers import *

import sys, os, subprocess



def parse_endmember_formula(file):
    """reads a SPud derived endmember description file and just extracts the formula"""
    libspud.load_options(file)
    formula =  libspud.get_option('/formula/name')
    libspud.clear_options()
    return formula


def parse_model_name(file):
    """reads a SPud derived endmember description file and just extracts the
    free energy model name
    """
    libspud.load_options(file)
    name =  libspud.get_option('/free_energy_model/name')
    libspud.clear_options()
    return name


def list_available_parameters(file):
    """ utility function to list out all parameter names and lengths of vector
    valued parameters
    """

    parameters = parse_all_parameters(file)
    scalars = [ param  for param in parameters if param['rank'] == 'Scalar']
    vectors = [ param  for param in parameters if param['rank'] == 'Vector']

    print('Scalar Parameters')
    fmt='    {:<10.10s} {:13.6e} {:<14.14s}'
    for scalar in scalars:
        print(fmt.format(scalar['name'],scalar['value'],scalar['units']))
    print('Vector Parameters')
    for vector in vectors:
        for i in range(vector['size']):
            print('    {:<0.10s}[{}] {:19.6e} {:<15.15s}'.
                  format(vector['name'],i,vector['value'][i],vector['units'][i]))


def init_dictionary():
    """Initializes dictionary with endmember name, formula and models"""
    dict = {}
    dict['name'] = libspud.get_option('name/name')
    dict['formula'] = libspud.get_option('formula/name')
    try:
        dict['reference'] = libspud.get_option('/reference')
    except libspud.SpudKeyError:
        dict['reference'] = None
    return dict


def parse_free_energy_model():
    """ parse the free_energy model part of the spud file and
    return a dictionary including name, lists of model variables and Parameters
    """

    # nested dictionary for model
    model = {}
    model['name'] = libspud.get_option('free_energy_model/name')

    # collect list of variables
    str = 'free_energy_model/variable'
    nvar = libspud.option_count(str)
    variables = []
    for i in range(nvar):
        variables.append(parse_variable(str+'[{}]'.format(i)))

    model['variables'] = variables

    # collect list of variables
    str = 'free_energy_model/parameter'
    nvar  = libspud.option_count(str)
    params = []
    for i in range(nvar):
        params.append(parse_parameter(str+'[{}]'.format(i)))

    model['parameters'] = params
    return model

def parse_potential(path,dict,global_dict,verbose=True,simplify=False):
    """Reads a single potential component of an endmember options tree and returns
    single sympy expression with all non-active parameters and functions
    substituted in.

    Parameters
    ----------
    path: python string top path of an endmember potential option tree
    dict:  Endmember dictionary containing lists of variables, parameters and subs_dict
    verbose: optional flag for printing progress
    simplify: optional boolean to attempt simplification of expression on initial read
        defaults to false.  However,  if parameter substitution is required, simplification is always called
        on the final expression.

    Output
    --------
    dictionary of optimized potential with fields
    potential['name']: python string with name of potential
    potential['expression']: optimized sympy expression for potential

    Examples
    --------
    potential_dict  = parse_potential('potential[0]',subs_dict)
    """

    subs_dict = dict['subs_dict']

    potential = {}
    potential['name'] = libspud.get_option(path+'/name')
    potential['symbol']= sym.Symbol(potential['name'])

    if verbose:
        print('Processing potential {}...'.format(potential['name']),end='')

    # sequentially evaluate explicit functions with substitution and build up
    # expression
    n_func = libspud.option_count(path+'/function')
    explicit_functions =[]
    functions=[]
    for i in range(n_func):
        str = path+'/function[{}]'.format(i)
        function = parse_function(str,dict,global_dict)
        #print('\tProcessed {} local function {}'.format(function['type'],function['name']))
        #print(function)
        if function['type'] == 'explicit':
            # add the explicit function symbol to the subs dictionary
            global_dict.update({function['name']:function['expression']})
            explicit_functions.append(function)

        else:
            functions.append(function)
    potential['functions']= functions
    potential['explicit_functions'] = explicit_functions


    expression = parse_expression(path, potential['name'],global_dict)
    if simplify:
        if verbose:
            print('Simplifying Expression...',end='')
        expression = expression.simplify()
    if len(subs_dict) > 0:
        if verbose:
            print('Substituting parameters and Simplifying...',end = '')
        expression = expression.subs(subs_dict).simplify()

    potential['expression'] = expression
    global_dict.update({ potential['name']:potential['expression']})

    if verbose:
        print('done')

    return potential

def parse_implicit_function(path,dict,global_dict):
    """Reads a single implicit_function component of an endmember options tree and returns
    a dictionary containing components of an implicit function

    Parameters
    ----------
    path: str: spud path to the top of an implicit function   option tree
    global_dict:  Endmember dictionary containing lists of variables, parameters and subs_dict for exec

    Output
    --------
    dictionary of implicit_function  with fields
    implicit_function['name']: python string with name of implicit function
    implicit_function['variable']:  sympy.symbol of implicit function
    implicit_function['residual']:  sympy expression for F, s.t. F(name)=0 is the solution
    implicit_function['initial_guess']: initial guess for newton solver

    Examples
    --------
    implicit_function  = parse_implicit_function(path,dict,global_dict)
    """

    implicit_function = {}
    name = libspud.get_option(path+'/name')
    implicit_function['name'] = name
    implicit_function['variable']=  parse_expression(path, name, global_dict)
    global_dict.update({name:sym.Function(name)})

    spud_path = '{}/{}'.format(path,'residual')
    name = libspud.get_option(spud_path+'/name')
    implicit_function['residual'] = parse_expression(spud_path,name,global_dict)

    spud_path = '{}/{}'.format(path, 'initial_guess')
    name = libspud.get_option(spud_path + '/name')
    implicit_function['guess'] = parse_expression(spud_path, name, global_dict)

    return implicit_function





def read_endmember(file,active=None,flatten_vectors=True,
                   flatten_potentials=True):
    """Reads a SPuD derived endmember description file (.emml file)
    and returns a nested python dictionary of data, parameters and
    sympy expressions that can be passed on to various code-generation
    schemesself.

    On input file is the name of an .emml files
    On output, the function returns a python dictionary

    Uses the libspud to read spud files

    Parameters
    ----------
    file : string containing name of endmember markup language file (.emml)
    active:  Flag or dictionary describing the active set of parameters  thar
            will be compiled into the eventual calibration code as variables
            if active == None:  all parameter values are substituted in as numbers
            if active == 'All',  all parameters are assumed to be active
            otherwise active is a list of parameter names to be made active
    flatten_vectors: boolean to determine how vector valued parameters are returned,
        e.g if p as a vector valued parameter with length n,
        flatten_vectors = True returns n parameters named p_i for i in range(n)
    flatten_potentials: boolean.  If true just returns a single potential that
        is the sum of all listed potentials


    Returns
    -------
    dict : Nested dictionary with fields
        ['name']
        ['formula']
        ['reference'] String including URL of reference for model (optional)
        ['model']['name'] ('Gibss, Helmholtz otherwise')
        ['model']['variables'] [list of Model variables as extended parameters]
        ['parameters'] [ list of all parameters]
        ['implicit_functions'] [ list of implicit_function dictionaries ]
        ['active_params'] [ names of active parameters (or "All" or None)]
        ['subs_dict'][ dictionary for substituting values for non-active parameters]
        [potentials] [ list of potential dictionaries]

    potentialdictionary  has structure
        ['name']
        ['symbol'] sympy symbol for potential
        ['Implicit Functions'] [list of structures for constructing implicit functions]
        ['Expression'] sympy expression with all non-active parameters subbed in

    if flatten_potentials=True.  Only 1 potential is returned that is the sum of all the sub-potentials

    Examples
    --------

    >>> dict = spudio.read_endmember('file.emml')
    >>> print (dict)
    >>> dict = spudio.read_endmember('file.emml',active="All")
    >>> print (dict)
    >>> params = ['H0', 'V0', 'S0']
    >>> dict = spudio.read_endmember('file.emml',active=params)

    """
    assert(isinstance(flatten_potentials,(bool)))
    assert(isinstance(flatten_vectors,(bool)))



    # Extract all parameters
    parameters = parse_all_parameters(file)

    # load emml files (will raise exception of no appropriate file)
    libspud.load_options(file)

    #initialize dictionary with name, formula, model and any global parameters
    dict = init_dictionary()

    # add filename to dictionary
    dict['filename'] = (file.split('/')[-1]).split('.')[0]


    #FIXME: this is klutzy,  I'm extracting global variables from the free energy model
    # and then discarding them because I've already extracted all the parameters
    model = parse_free_energy_model()
    model.pop('parameters')
    variables = model['variables']
    dict['model']=model
    dict['parameters'] = parameters

    # sort out active and non-active parameters
    s,a = set_subs_parameters(parameters,active)
    dict['active_parameters'] = a
    dict['subs_dict'] = s

    # set up global_dict for exec to understand all parameters, variables, functions and potentials
    # set up a global dictionary of symbols for resolving symbols in sympy
    global_dict={}
    global_dict["sym"]=sym
    global_dict.update(sym.__dict__)
    # extend the global dictionary to support coder external functions
    global_dict.update(coder.get_external_functions())
    global_dict.update({parameter['name']:parameter['symbol'] for parameter in parameters})
    global_dict.update({variable['name']:variable['symbol'] for variable in variables})

    #  parse implicit functions
    dict['implicit_functions'] = []
    spud_path = 'functions/implicit_function'
    if libspud.have_option(spud_path):
        print('processing implicit functions')
        # read implicit functions
        n_ifunc = libspud.option_count(spud_path)
        for i in range(n_ifunc):
            spud_path = 'functions/implicit_function[{}]'.format(i)
            ifunc = parse_implicit_function(spud_path, dict, global_dict)
            dict['implicit_functions'].append(ifunc)

    # loop over potentials and accumulate
    n_pot = libspud.option_count('potential')
    potentials = []
    for i in range(n_pot):
        potential = parse_potential('potential[{}]'.format(i),dict,global_dict)
        potentials.append(potential)

    if flatten_potentials:
        p_tot = {}
        if dict['model']['name'] == 'Gibbs':
            p_tot['name']= 'G'
        elif dict['model']['name'] == 'Helmholtz':
            p_tot['name'] = 'A'
        else:
            p_tot['name'] = dict['model']['name']
            p_tot['symbol'] = sym.Symbol(p_tot[name])

        p_tot['functions']=[]
        expression = sym.S.Zero

        for potential in potentials:
            p_tot['functions'].append(potential['functions'])
            expression += potential['expression']

        p_tot['expression'] = expression.simplify()

        potentials.append(p_tot)
        print('Processed flattened potential {}'.format(p_tot['name']))

    dict['potentials'] = potentials
    libspud.clear_options()
    return dict


def split_parameters(model_dict):
    parameters = model_dict['parameters'].copy()
    model_params = []
    reference_params = ['T_r', 'P_r', 'V_r']
    for p in model_dict['parameters']:
        if p['name'] in reference_params:
            model_params.append(p)
            parameters.remove(p)
    return model_params, parameters


def write_external_function(spud_path, efunc):
    try:
        libspud.add_option(spud_path)
    except libspud.SpudNewKeyWarning as err:
        pass

    # add Variables


def write_endmember(model_dict, filename=None, path=None):
    """
    Writes a SPuD derived endmember description file (.emml file)
    from a nested python dictionary of data, parameters and
    sympy expressions that can be passed on to various code-generation
    schemes.

    On input:  a complete model_dictionary for endmembers
    On output, the function returns a python dictionary

    Uses the libspud to read spud files

    Parameters
    ----------
    model_dict : Python dictionary describing endmember model
    filename:  optional name of file. Will default to model_dict['name'].emml
    path:      optional directory path to write file (otherwise CWD)


    Returns
    -------
    Nothing

    Examples
    --------

    >>>  spudio.write_endmember(model_dict)
    """
    assert (isinstance(model_dict, dict))

    md = model_dict.copy()

    # Path to templates
    tcg_home = os.environ.get('THERMOCODEGEN_HOME')
    if tcg_home is None:
        raise OSError('Environment variable THERMOCODEGEN_HOME is not set')

    template_dir = tcg_home + '/share/thermocodegen/templates/spud'

    ## FIXME:  should have better error checking to see if model_dict is valid
    # Check model type and load appropriate template directory

    try:
        libspud.clear_options()
    except:
        pass

    try:
        model_name = md['model']['name']
        spud_template = '{}/StdStateModel_{}.emml'.format(template_dir, model_name)
        libspud.load_options(spud_template)
        # libspud.print_options()
    except KeyError as err:
        print('Only supports Energy models, Gibbs and Helmholtz', err)
        raise KeyError

    # set pre-assigned values in template
    write_attribute_name('/name',md['name'])
    write_attribute_name('/formula',md['formula'])
    write_str('/reference',md['reference'])

    # set model specific parameters and pop from parameter list
    model_params, parameters = split_parameters(md)

    for p in model_params:
        spud_path = "/free_energy_model::{}/parameter::{}".format(model_name, p['name'])
        write_parameter(spud_path,p)
        #spud_path = "/free_energy_model::{}/parameter::{}/rank::Scalar/value".format(model_name, p['name'])
        # try:
        #     libspud.set_option(spud_path, p['value'])
        # except:
        #     pass

    # now populate global parameters
    for p in parameters:
        spud_path = "/parameters/parameter::{}".format(p['name'])
        write_parameter(spud_path,p)

    # write out implicit functions if they exist
    for ifunc in md['implicit_functions']:
        spud_path = '/functions/implicit_function::{}'.format(ifunc['name'])
        write_implicit_function(spud_path,ifunc)

    # write out external functions if they exist
    # for efunc in md['external_functions']:
    #     spud_path = '/functions/external_function::{}'.format(efunc['name'])
    #     write_explicit_functions(spud_path, efunc)

    # write out Potentials
    for pot in md['potentials']:
        spud_path = '/potential::{}'.format(pot['name'])
        write_potential(spud_path,pot)

    # Output the option files
    if filename is None:
        filename = md['name'] + '.emml'

    if path is not None:
        filename = "{}/{}".format(path, filename)

    # libspud.print_options()
    libspud.write_options(filename)
    libspud.clear_options()

    return None





if __name__ == "__main__":
    #list_available_parameters('test_GD.emml')

    #active = ['H0','V0','S0']
    #sdict = read_endmember('test_GD.emml',active=active)
    #print(dict)

    #dictD = read_endmember('test_GD.emml')

    # read in the pickled dictionary and try to write it
    import pickle

    from glob import glob
    filenames = glob('*.pkl')
    filenames = ['Potassium_Feldspar_Berman.pkl', 'Forsterite_Stixrude_A0Kcal.pkl', 'Na_1_Cation_Hkf_C.pkl']
    for filename in filenames:
        print(filename)
        with open(filename,'rb') as file:
            model_dict = pickle.load(file)

        write_endmember(model_dict)
        filename = '{}.emml'.format(model_dict['name'].title())

        list_available_parameters(filename)

        params = parse_all_parameters(filename)
        units = [p['units'] for p in params]

        e_dict = read_endmember(filename,active='all',flatten_potentials=False)

    # check on the implicit function stuff
    filename = 'Potassium_Feldspar_Berman_Bm.emml'
    e_dict = read_endmember(filename,active='all',flatten_potentials=False)



