py::class_<${name},  std::shared_ptr<${name}> >(m, "${name}", py::module_local())
    .def(py::init<>())

    // Coder interfaces
    .def("identifier", &${name}::identifier,"identifier")
    .def("name", &${name}::name,"phase name")
    .def("tcg_build_version", &${name}::tcg_build_version, "Version of TCG used to build endmember")
    .def("tcg_build_git_sha", &${name}::tcg_build_git_sha, "Git SHA of TCG used to build endmember")
    .def("tcg_generation_version", &${name}::tcg_generation_version, "Version of TCG used to generate endmember")
    .def("tcg_generation_git_sha", &${name}::tcg_generation_git_sha, "Git SHA of TCG used to generate endmember")
    .def("formula", &${name}::formula,"formula",py::arg("T"),py::arg("P"),py::arg("n"))
    .def("conv_elm_to_moles", &${name}::conv_elm_to_moles,"convert elements to moles",py::arg("e"))
    .def("conv_elm_to_tot_moles", &${name}::conv_elm_to_tot_moles,"convert elements to total moles",py::arg("e"))
    .def("conv_elm_to_tot_grams", &${name}::conv_elm_to_tot_grams,"convert elements to total grams",py::arg("e"))
    .def("conv_moles_to_elm", &${name}::conv_moles_to_elm,"convert moles to elements",py::arg("n"))
    .def("conv_moles_to_tot_moles", &${name}::conv_moles_to_tot_moles,"convert moles to total moles",py::arg("n"))
    .def("conv_moles_to_mole_frac", &${name}::conv_moles_to_mole_frac,"convert moles to mole fraction",py::arg("n"))
    .def("test_moles", &${name}::test_moles,"test moles are valid",py::arg("n"))
    .def("endmember_number", &${name}::endmember_number)
    .def("endmember_name", &${name}::endmember_name, py::arg("i"))
    .def("endmember_formula", &${name}::endmember_formula, py::arg("i"))
    .def("endmember_mw", &${name}::endmember_mw, py::arg("i"))
    .def("endmember_elements", &${name}::endmember_elements, py::arg("i"))
    .def("species_number", &${name}::species_number)
    .def("species_name", &${name}::species_name, py::arg("i"))
    .def("species_formula", &${name}::species_formula, py::arg("i"))
    .def("species_mw", &${name}::species_mw, py::arg("i"))
    .def("species_elements", &${name}::species_elements, py::arg("i"))
    .def("g", &${name}::g,"total Gibbs Free energy of phase (J)",py::arg("T"),py::arg("P"),py::arg("n"))
    .def("dgdt", &${name}::dgdt,py::arg("T"),py::arg("P"),py::arg("n"))
    .def("dgdp", &${name}::dgdp,py::arg("T"),py::arg("P"),py::arg("n"))
    .def("dgdn", &${name}::dgdn,py::arg("T"),py::arg("P"),py::arg("n"))
    .def("d2gdt2", &${name}::d2gdt2,py::arg("T"),py::arg("P"),py::arg("n"))
    .def("d2gdtdp", &${name}::d2gdtdp,py::arg("T"),py::arg("P"),py::arg("n"))
    .def("d2gdp2", &${name}::d2gdp2,py::arg("T"),py::arg("P"),py::arg("n"))
    .def("d2gdndt", &${name}::d2gdndt,py::arg("T"),py::arg("P"),py::arg("n"))
    .def("d2gdndp", &${name}::d2gdndp,py::arg("T"),py::arg("P"),py::arg("n"))
    .def("d2gdn2", &${name}::d2gdn2,py::arg("T"),py::arg("P"),py::arg("n"))
    .def("d3gdt3", &${name}::d3gdt3,py::arg("T"),py::arg("P"),py::arg("n"))
    .def("d3gdp3", &${name}::d3gdp3,py::arg("T"),py::arg("P"),py::arg("n"))
    .def("d3gdt2dp", &${name}::d3gdt2dp,py::arg("T"),py::arg("P"),py::arg("n"))
    .def("d3gdndt2", &${name}::d3gdndt2,py::arg("T"),py::arg("P"),py::arg("n"))
    .def("d3gdtdp2", &${name}::d3gdtdp2,py::arg("T"),py::arg("P"),py::arg("n"))
    .def("d3gdndtdp", &${name}::d3gdndtdp,py::arg("T"),py::arg("P"),py::arg("n"))
    .def("d3gdn2dt", &${name}::d3gdn2dt,py::arg("T"),py::arg("P"),py::arg("n"))
    .def("d3gdndp2", &${name}::d3gdndp2,py::arg("T"),py::arg("P"),py::arg("n"))
    .def("d3gdn2dp", &${name}::d3gdn2dp,py::arg("T"),py::arg("P"),py::arg("n"))
    .def("d3gdn3", &${name}::d3gdn3,py::arg("T"),py::arg("P"),py::arg("n"))
    .def("v", &${name}::v,"Molar Volume",py::arg("T"),py::arg("P"),py::arg("n"))
    .def("s", &${name}::s,"Molar entropy",py::arg("T"),py::arg("P"),py::arg("n"))
    .def("alpha", &${name}::alpha,"Coefficent of thermal expansion for the phase",py::arg("T"),py::arg("P"),py::arg("n"))
    .def("cv", &${name}::cv,py::arg("T"),py::arg("P"),py::arg("n"))
    .def("cp", &${name}::cp,"Molar Heat capacity of phase at constant pressure",py::arg("T"),py::arg("P"),py::arg("n"))
    .def("dcpdt", &${name}::dcpdt,py::arg("T"),py::arg("P"),py::arg("n"))
    .def("beta", &${name}::beta,py::arg("T"),py::arg("P"),py::arg("n"))
    .def("K", &${name}::K,py::arg("T"),py::arg("P"),py::arg("n"))
    .def("Kp", &${name}::Kp,py::arg("T"),py::arg("P"),py::arg("n"))
    .def("get_param_number", &${name}::get_param_number,"number of active parameters")
    .def("get_param_names", &${name}::get_param_names,"active parameter names")
    .def("get_param_units", &${name}::get_param_units,"active parameter units")
    .def("get_param_units", &${name}::get_param_units,"active parameter units")
    .def("get_param_values", py::overload_cast<>(&${name}::get_param_values),"get active parameter values")
    .def("set_param_values", &${name}::set_param_values,"set active parameter values",py::arg("values"))
    .def("get_param_value", &${name}::get_param_value,"return value for a particular active parameter",py::arg("index"))
    .def("set_param_value", &${name}::set_param_value,"set value for a particular active parameter",py::arg("index"),py::arg("value"))
    .def("dparam_g", &${name}::dparam_g,"dparam_g",py::arg("T"),py::arg("P"),py::arg("n"),py::arg("index"))
    .def("dparam_dgdt", &${name}::dparam_dgdt,"dparam_dgdt",py::arg("T"),py::arg("P"),py::arg("n"),py::arg("index"))
    .def("dparam_dgdp", &${name}::dparam_dgdp,"dparam_dgdp",py::arg("T"),py::arg("P"),py::arg("n"),py::arg("index"))
    .def("dparam_dgdn", &${name}::dparam_dgdn,"dparam_dgdn",py::arg("T"),py::arg("P"),py::arg("n"),py::arg("index"))
    .def("dparam_d2gdt2", &${name}::dparam_d2gdt2,"dparam_d2gdt2",py::arg("T"),py::arg("P"),py::arg("n"),py::arg("index"))
    .def("dparam_d2gdtdp", &${name}::dparam_d2gdtdp,"dparam_d2gdtdp",py::arg("T"),py::arg("P"),py::arg("n"),py::arg("index"))
    .def("dparam_d2gdp2", &${name}::dparam_d2gdp2,"dparam_d2gdp2",py::arg("T"),py::arg("P"),py::arg("n"),py::arg("index"))
    .def("dparam_d3gdt3", &${name}::dparam_d3gdt3,"dparam_d3gdt3",py::arg("T"),py::arg("P"),py::arg("n"),py::arg("index"))
    .def("dparam_d3gdt2dp", &${name}::dparam_d3gdt2dp,"dparam_d3gdt2dp",py::arg("T"),py::arg("P"),py::arg("n"),py::arg("index"))
    .def("dparam_d3gdtdp2", &${name}::dparam_d3gdtdp2,"dparam_d3gdtdp2",py::arg("T"),py::arg("P"),py::arg("n"),py::arg("index"))
    .def("dparam_d3gdp3", &${name}::dparam_d3gdp3,"dparam_d3gdp3",py::arg("T"),py::arg("P"),py::arg("n"),py::arg("index"))

    // ThermoCodegen interfaces
    .def("endmembers", &${name}::endmembers,"vector of pointers to endmembers")
    .def("abbrev", &${name}::abbrev,"official phase abbreviation")
    .def("mu", &${name}::mu,"chemical potential dG/dn (J/mol)",py::arg("T"),py::arg("P"),py::arg("x"))
    .def("dmu_dT", &${name}::dmu_dT,py::arg("T"),py::arg("P"),py::arg("x"))
    .def("dmu_dP", &${name}::dmu_dP,py::arg("T"),py::arg("P"),py::arg("x"))
    .def("dmu_dc", &${name}::dmu_dc,"change in all chemical potentials for a change in composition",
           py::arg("T"),py::arg("P"),py::arg("c"))
    .def("ds_dc", &${name}::ds_dc,"change in specific entropy for a change in composition",
           py::arg("T"),py::arg("P"),py::arg("c"))
    .def("dv_dc", &${name}::dv_dc,"change in specific volume for a change in composition",
           py::arg("T"),py::arg("P"),py::arg("c"))
    .def("Mass", &${name}::Mass,"Molar Mass",py::arg("x"))
    .def("rho", &${name}::rho,"density of phase (T,P,C) in weird units ",py::arg("T"),py::arg("P"),py::arg("c"))
    .def("drho_dT", &${name}::drho_dT,py::arg("T"),py::arg("P"),py::arg("c"))
    .def("drho_dP", &${name}::drho_dP,py::arg("T"),py::arg("P"),py::arg("c"))
    .def("drho_dc", &${name}::drho_dc,py::arg("T"),py::arg("P"),py::arg("c"))
    .def("c_to_x",  py::overload_cast<std::vector<double>& >  (&${name}::c_to_x, py::const_),
             "convert weight fractions to mole fractions",py::arg("c"))
    .def("x_to_c",  py::overload_cast<std::vector<double>& > (&${name}::x_to_c, py::const_),
             "convert mole fractions to weight fractions",py::arg("x"));
