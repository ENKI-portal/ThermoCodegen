# -*- coding: utf-8 -*-
import numpy as np
import sympy as sym
import os
import warnings

from collections import OrderedDict
from . import calc_coeffs as cc
from thermocodegen.codegen.printer import SubCodePrinter
from thermocodegen.spudio.reaction import get_files_parsers, fetch_database, \
                                          get_phase_dict
from .. import spudio
import tarfile
import tempfile


class Reaction(object):
    """
    Class deals with C++ code generation for a set of reactions.

    Parameters
    ----------
    model_dict : dict, optional
        A nested dictionary containing the information necessary to describe
        a set of reactions.

    total_reactions : int, optional
        Total number of reactions.

    phase_names : list[str], optional
        List of phase names.

    database : url, optional
        A url that specifies the location of a thermodynamic database tarball.

    Attributes
    ----------

    model_dict : dict
        A nested dictionary containing the information necessary to describe
        a set of reactions.

    phases : list[dict]
        A list of nested dictionaries that describe each individual phase.

    J : int
        Number of reactions.

    N : int
        Number of phases involved in the reactions.

    Kmax : int
        Maximum number of endmembers appearing in a phase.

    reactions : list[dict]
        A list of dictionaries that describe each individual reaction.

    printer : CXX11CodePrinter
        Instance of a SymPy code printer class used in code generation.

    T : SymPy symbol
        SymPy symbol for temperature T.

    P : SymPy symbol
        SymPy symbol for pressure T.

    C : SymPy symbol
        SymPy symbol for concentration matrix C.

    Phi : SymPy symbol
        SymPy symbol for phase fraction vector Phi.

    Notes
    -----

    This class can be initialized via either of the ``from_database`` or
    the ``from_dict`` class methods. For example

    >>> phase_names = [ 'Olivine', 'Wadsleyite', 'Ringwoodite']
    >>> db = https://zenodo.org/record/4837819/files/MgFeSiO4_Stixrude.tar.gz
    >>> rxn = Reaction().from_database(total_reaction=4, phase_names=phase_names, database=db)

    uses an already constructed Thermodynamic database that includes at least the phases
    described in `phase_names`  (along with their endmembers) to construct a set of 4 reactions
    over the space of endmembers and phases.

    On instantiation,  the full model dictionary can be viewed with

    >>> rxn.model_dict

    Alternatively,  the reaction object can be constructed using

    >>> rxn = Reaction().from_dict(model_dict)

    where `model_dict` is from a previously created :class:`Reaction` object.  This form is
    usually only used in scripts.

    If neither of these class methods are called on instantiation, then the
    default behaviour will use a model dictionary if one is passed as a
    parameter, otherwise it will require each of total_reactions, phase_names
    and database to be given as parameters.
    """

    def __init__(self, model_dict=None, total_reactions=None,
                 phase_names=None, database=None):
        self.model_dict = None
        self.phases = None
        self.J = None
        self.N = None
        self.Kmax = None
        self.reactions = []
        self.printer = self.init_code_printer()

        if model_dict:
            self.init_model_from_dict(model_dict)
        elif all(arg is not None for arg in [total_reactions,
                                             phase_names, database]):
            self.init_model_dict(total_reactions, phase_names, database)
        else:
            raise Exception('Please provide a model dictionary or each of the '
                            'following: the number of reactions, a list of '
                            'phases and a valid database url.')

        # Set SymPy variable symbols
        self.T = sym.Symbol('T')
        self.P = sym.Symbol('P')
        self.C = sym.MatrixSymbol('C', self.N, self.Kmax)
        self.Phi = sym.MatrixSymbol('Phi', self.N, 1)

        # Set SymPy symbol for the affinity (A)
        self.A = sym.MatrixSymbol('A', self.J, 1)

        # Variables to be used in code generation
        self._nu = None
        self._indices = None
        self._C0 = None
        self._M = None
        self._nu_matrix = None
        self._nu_m_matrix = None

        # Dictionary for substituting dAj_dCik expressions
        self._dA_dC = None

        # Set list of mass transfer rates per phase
        self._Gamma_phases = None

        # Set ragged array of mass transfer rates per component
        self._Gamma_components = None


    @classmethod
    def from_database(cls, total_reactions=None,
                      phase_names=None, database=None):
        """Initializes object from a thermodynamic database.

        Parameters
        ----------
        total_reactions : int
            Total number of reactions.

        phase_names : list[str]
            List of phase names.

        database : url
            A url that specifies the location of a thermodynamic database tarball.

        Examples
        --------

        Example using a locally generated database:

        >>> phase_names = ['Olivine', 'Liquid']
        >>> db = file://fo_fa.tar.gz
        >>> rxn = Reaction.from_database(total_reactions=2, phase_names=phase_names, database=db)

        """
        assert (total_reactions > 0), 'Number reactions must be greater than 0'
        assert (len(phase_names) > 0), 'At least one phase must be given'
        return cls(None, total_reactions=total_reactions,
                   phase_names=phase_names, database=database)

    @classmethod
    def from_dict(cls, model_dict=None):
        """Initializes object from a model dictionary.

        Parameters
        ----------
        model_dict : dict
            A nested dictionary containing the information necessary to describe
            a set of reactions.

        Note
        ----

        This method is usually not called interactively

        """
        return cls(model_dict=model_dict)

    def init_model_from_dict(self, model_dict):
        """Initializes instance variables from a model dictionary."""
        self.model_dict = model_dict
        self.J = model_dict['J']
        self.reactions = model_dict['reactions']
        self.N = len(model_dict['phases'])
        self.phases = model_dict['phases']
        K = [len(self.phases[i]['endmembers']) for i in range(self.N)]
        self.Kmax = max(K)

    def init_model_dict(self, total_reactions, phase_names, database):
        """Initializes instance variables from a thermodynamic database."""
        self.J = total_reactions
        self.N = len(phase_names)
        self.phases = self.get_phase_dict(database, phase_names)
        self.Kmax = max([len(p['endmembers']) for p in self.phases])

        keys = ['name', 'database', 'reference']
        self.model_dict = OrderedDict.fromkeys(keys, None)
        self.model_dict['parameters'] = []
        self.model_dict['reactions'] = []
        self.model_dict['phases'] = self.phases
        self.model_dict['database'] = database
        self.model_dict['J'] = total_reactions

    def get_phase_dict(self, database, phase_names):
        """Retrieves a phase dictionary from a thermodynamic database."""
        with tempfile.TemporaryDirectory() as tmp_dir:
            path = fetch_database(database, tmp_dir)
            tar = tarfile.open(path, mode="r")
            tar.extractall(path=tmp_dir)
            tar.close()

            _files, phml_dict = get_files_parsers(tmp_dir, '.phml')
            _files, emml_dict = get_files_parsers(tmp_dir, '.emml')

            endmember_dict = {e.name: e for e in emml_dict}
            phase_dict = {p.name: p for p in phml_dict}
            db_dict = {'endmembers': endmember_dict, 'phases': phase_dict}

            #FIXME:  potential name clash, this get_phase_dict is from spudio, but it looks recursive
            try:
                phases = [get_phase_dict(p, db_dict) for p in phase_names]
            except KeyError as error:
                print('The phase {} is not present in database'.format(error))

            return phases

    def init_code_printer(self):
        """Specify user functions in SubCodePrinter."""
        func_dict = {}
        func_dict['A'] = 'A'
        func_dict['rho'] = 'rho'
        printer = SubCodePrinter(settings={'user_functions': func_dict})

        return printer

    def add_reaction_to_model(self, name, reactants, products,
                              expression, params, symbol=None):
        """Adds a new reaction to the model_dict.

        Parameters
        ----------
        name : str
            The name for this reaction.

        reactants : list
            A list of tuples of strings, where each tuple is of the form

                (name, phase, endmember)

            where 'name' represents the user defined name of the reactant,
            'phase' is name of the phase, and 'endmember' is the name of
            the endmember in this phase. The 'phase' and 'endmember' should be
            consistent with the corresponding names in the thermodynamic
            database.

        products : list
            A list of tuples of strings, where each tuple is of the form

                (name, phase, endmember)

            where 'name' represents the user defined name of the product,
            'phase' is name of the phase, and 'endmember' is the name of
            the endmember in this phase. The 'phase' and 'endmember' should be
            consistent with the corresponding names in the thermodynamic
            database.

        expression : SymPy expression
            A SymPy expression that describes the reaction rate for this
            reaction.

        params : list
            A list of tuples of the form

                (param, units, symparam)

            where 'param' is a string representing the name of the parameter,
            'units' is a string describing its units, and 'symparam' is
            its corresponding SymPy symbol.

        symbol : SymPy symbol, optional
            A SymPy symbol attached to this reaction rate. If not specificied,
            the default symbol is given by the 'name' parameter.

        """
        reaction = {}
        reaction['name'] = name
        reaction['reactants'] = []
        reaction['products'] = []
        reaction['expression'] = expression

        for (_name, _phase, _endmember) in reactants:
            reaction['reactants'].append(dict(name=_name,
                                              phase=_phase,
                                              endmember=_endmember))

        for (_name, _phase, _endmember) in products:
            reaction['products'].append(dict(name=_name,
                                             phase=_phase,
                                             endmember=_endmember))

        param_names = [p['name'] for p in self.model_dict['parameters']]
        for (_name, _units, _symbol) in params:
            if _name in param_names:
                print('Overriding {} -- already '
                      'exists in model_dict'.format(_name))
                for k, param in enumerate(self.model_dict['parameters']):
                    if param['name'] == _name:
                        del self.model_dict['parameters'][k]

            self.model_dict['parameters'].append(dict(name=_name,
                                                      rank='Scalar',
                                                      size=1,
                                                      value=None,
                                                      units=_units,
                                                      symbol=_symbol))

        if not symbol:
            symbol = sym.Symbol(name)

        self.model_dict['reactions'].append(reaction)

    def get_values(self):
        """Returns a dictionary of current {name: value} pairs for settable
        values and parameters in the model_dict.

        Returns
        -------


            """
        md = self.model_dict
        value_dict = {}
        keys = ['name', 'database', 'reference', 'phases']
        for key in keys:
            value_dict.update({key: md[key]})

        # Extract parameters
        params = self.model_dict['parameters']
        name = [p['name'] for p in params]
        values = [p['value'] for p in params]
        value_dict.update(dict(zip(name, values)))

        return value_dict

    def set_values(self, values_dict):
        """Sets values in the model_dict from a dictionary of {name: value}
        pairs.

        Parameters
        ----------
        values_dict : dict
            a dictionary of {names: values} to be set in the model dictionary.

        Examples
        ---------

        >>> values_dict = {'name': 'fo_fa_binary', 'database': 'fo_fa_db.tar.gz', 'phases': ['Olivine', 'Liquid']}
        >>> rxn.set_values(values_dict)

        """
        vdict = values_dict.copy()
        keys = ['name', 'database', 'reference', 'phases']
        for key in keys:
            try:
                self.model_dict[key] = vdict[key]
            except KeyError as err:
                print('Warning KeyError: ', err)

            vdict.pop(key, None)

        # Set parameters
        for param in self.model_dict['parameters']:
            if param['name'] in vdict.keys():
                param['value'] = float(vdict[param['name']])

    def check_model_dict(self):
        """Checks model_dict for errors.

        Raises
        ------
        ValueError
            If there are any currently empty keys or if the number
            of reactions is inconsistent with 'total_reactions'.
        """
        # Check there are no empty keys
        empty_keys = [k for k, v in self.model_dict.items() if not v]
        if empty_keys:
            raise ValueError('The following keys are not set: {}'.format(
                             empty_keys))

        # Check number of reactions
        if len(self.reactions) != self.J:
            raise ValueError('Expected {} reactions, got {}'.format(
                             self.J, len(self.reactions)))

    def to_xml(self, filename=None, path=None, validate=True, verbose=False):
        """
        write and optionally validate spud xml file from reaction model

        Parameters
        ----------

        filename : str, optional
            name of output file,  should include schema extension ``.phml``
            if None,  the filename will default to ``model_dict['name'].phml``
        path : str, optional
            path to write output file. defaults to current directory
        validate: bool
            if True:  checks output spudfile and spud schema using diamond
        verbose: bool
            verbosity argument to be passed to validation scheme

        Returns
        -------

        filename: str
        """

        schema_ext = 'rxml'
        if filename is None:
            filename = '{}.{}'.format(self.model_dict['name'], schema_ext)
        else:
            ext = filename.split('.')[-1]
            if ext != schema_ext:
                raise ValueError('filename: {} does not have proper schema extension ({})'
                                 .format(filename, schema_ext))
        if path is None:
            path = os.curdir
        # pass to spudio write_reactions
        spudio.write_reactions(self.model_dict, filename=filename, path=path)

        if validate:
            filepath = '{}/{}'.format(path, filename)
            isvalid = spudio.validate_spudfile(filepath, verbose=verbose)
            if not isvalid:
                warnings.warn('{} does not parse cleanly with diamond'.format(filename))

        return filename

    def get_stoichiometric_coeffs(self,mass_weighted=False):
        """Returns a matrix of stoichiometric coefficients nu_ij."""
        nu = []
        for rx in self.reactions:
            members = rx['reactants'] + rx['products']
            formulae = [m[0] for m in members]
            nu.append(cc.stoichiometric_coeffs(formulae,mass_weighted))

        return nu

    def reaction_functions(self):
        """Returns a dictionary for C++ template substitution."""
        # First set variables required for code generation
        self._nu = self.get_stoichiometric_coeffs()
        self._indices = self.phase_endmember_indices()
        self._C0 = self.list_to_Carray(self.set_zeroC_matrix())
        self._M = self.init_M()
        self._nu_matrix = self.set_nu_matrix()
        self._nu_m_matrix = self.list_to_Carray(self.set_nu_m_matrix())
        self._dA_dC = self.dA_dC_expr()
        self._Gamma_phases = self.Gamma_i_expr()
        self._Gamma_components = self.Gamma_ik_expr()

        rx_funcs = {
            "name": self.model_dict['name'],
            "name_uppercase": self.model_dict['name'].upper(),
            "J": self.J,
            "phases": self.phase_list(),
            "Nphases": self.N,
            "C0": self._C0,
            "M": self.list_to_Carray(self._M),
            "nu": self._nu_matrix,
            "nu_m": self._nu_m_matrix,
            "A": self.Affinity(),
            "dA_dT": self.dA_dT(),
            "dA_dP": self.dA_dP(),
            "Gamma_i": self.Gamma_i(),
            "dGamma_i_dT": self.dGamma_i_dT(),
            "dGamma_i_dP": self.dGamma_i_dP(),
            "dGamma_phases_dPhi": self.dGamma_phases_dPhi(),
            "dGamma_i_dPhi": self.dGamma_i_dPhi(),
            "dGamma_phases_dC": self.dGamma_phases_dC(),
            "dGamma_i_dC": self.dGamma_i_dC(),
            "Gamma_components": self.Gamma_components(),
            "Gamma_ik": self.Gamma_ik(),
            "dGamma_components_dT": self.dGamma_components_dT(),
            "dGamma_ik_dT": self.dGamma_ik_dT(),
            "dGamma_components_dP": self.dGamma_components_dP(),
            "dGamma_ik_dP": self.dGamma_ik_dP(),
            "dGamma_ik_dC_i": self.dGamma_ik_dC_i(),
            "dGamma_ik_dPhi_i": self.dGamma_ik_dPhi_i(),
            "dGamma_ik_dC": self.dGamma_ik_dC(),
            "dGamma_ik_dPhi": self.dGamma_ik_dPhi(),
            "parameter_declarations": self.parameter_declarations(),
            "init_parameters": self.init_parameters(),
            "parameter_container": self.parameter_container(),
            "init_parameter_container": self.init_parameter_container()
            }

        # Add vector functions in dictionary
        rx_funcs.update(self.vector_functions())

        return rx_funcs

    def phase_endmember_indices(self):
        """Returns indices of phase and endmember as they appear in inputs."""
        indices = []
        for rx in self.reactions:
            rx_indices = []
            members = rx['reactants'] + rx['products']
            for m in members:
                phase_index = [i for i in range(self.N)
                               if self.phases[i]['name'] == m[1]][0]
                endmembers = self.phases[phase_index]['endmembers']
                endmember_index = [k for k in range(len(endmembers))
                                   if endmembers[k] == m[2]][0]
                rx_indices.append([phase_index, endmember_index])
            indices.append(rx_indices)

        return indices

    def parameter_declarations(self):
        """Add parameters to private members in header."""
        params = []
        for p in self.model_dict['parameters']:
            params.append('double {p};'.format(p=p['name']))

        params = '\n  '.join(params)

        return params

    def init_parameters(self):
        """Initialize parameter values through the constructor."""
        init_vals = []
        for p in self.model_dict['parameters']:
            init_vals.append('{p}({val})'.format(p=p['name'], val=p['value']))

        init_vals = (',\n  '.join(init_vals))*bool(init_vals)

        return init_vals

    def parameter_container(self):
        """Initialize keys in map container."""
        container = []
        for p in self.model_dict['parameters']:
            container.append('{'+'"{p}"'.format(p=p['name'])+', new double}')

        container = 'parameters({' + ', '.join(container) + '})' \
                    + ','*bool(container)

        return container

    def init_parameter_container(self):
        """Set map container values in the constructor."""
        init_container = []
        for p in self.model_dict['parameters']:
            name = p['name']
            init_container.append('parameters["{p}"] = &{p}'.format(p=name))

        init_container = ';\n  '.join(init_container) + ';'

        return init_container

    def phase_list(self):
        """String for std::vector of pointers to Phase objects."""
        phase_str = "std::vector<std::shared_ptr<Phase> > {"
        phase_list = ["std::make_shared<{}>()".format(p['name'])
                      for p in self.phases]
        phase_str += ",".join(phase_list) + " }"

        return phase_str

    def set_zeroC_matrix(self):
        """Initializes composition state as a ragged array of zeros."""
        C0 = []
        for phase in self.phases:
            C0.append(np.zeros(len(phase['endmembers'])).tolist())

        return C0

    def list_to_Carray(self, C):
        """Converts a list to a string representation of a C++ std::vector."""
        C_str = '{}'.format(C).replace('[', '{').replace(']', '}')

        return C_str

    def init_M(self):
        """Constructs ragged array of endmember molar masses (M)."""
        M = self.set_zeroC_matrix()
        for i, phase in enumerate(self.phases):
            M[i] = phase['Mik']

        return M

    def set_nu_matrix(self):
        """Constructs a C++ ragged array of molar coefficients (nu)."""
        # Initilize a ragged array of zeros
        nu_matrix = []
        for j in range(self.J):
            nu_matrix.append(self.set_zeroC_matrix())

        # Fill in non-zero indices from self._nu and self._indices
        for j, nu in enumerate(self._nu):
            for m, nuik in enumerate(nu):
                i = self._indices[j][m][0]
                k = self._indices[j][m][1]
                nu_matrix[j][i][k] = nuik

        # Convert to C++ array notation
        return self.list_to_Carray(nu_matrix)

    def set_nu_m_matrix(self):
        """Constructs a C++ ragged array of mass coefficients (nu_m)."""
        # Initilize a ragged array of zeros
        nu_m_matrix = []
        for j in range(self.J):
            nu_m_matrix.append(self.set_zeroC_matrix())

        # Fill in non-zero indices from self._nu and self._indices
        for j, nu in enumerate(self._nu):
            # Calculate mass of reaction (M_j)
            Mj = 0.
            for m, nuik in enumerate(nu):
                i = self._indices[j][m][0]
                k = self._indices[j][m][1]
                if nuik > 0:
                    Mj += nuik*self._M[i][k]

            # Calculate mass normalized coefficients (nu_m)
            for m, nuik in enumerate(nu):
                i = self._indices[j][m][0]
                k = self._indices[j][m][1]
                nu_m_ik = nuik*self._M[i][k]/Mj
                nu_m_matrix[j][i][k] = nu_m_ik

        return nu_m_matrix

    def dA_dC_expr(self):
        """Returns a dictionary of SymPy expressions for dAj_dCik."""
        # Create sympy functions and substitution dictionary for affinities
        J = self.J
        A = [sym.Function('A')(self.T, self.P, self.C, j) for j in range(J)]

        _dA_dC_dict = {}
        for j in range(J):
            for i, phase in enumerate(self.phases):
                for k in range(len(phase['endmembers'])):
                    # Substitute expression for dAj_dCik
                    key = A[j].diff(self.C[i, k])
                    _dA_dC_dict[key] = sym.Derivative(A[j], self.C[i, k])

        return _dA_dC_dict

    # deprecated (all methods that depend on this have been removed)
    def sum_nu_func_list(self, func='mu'):
        """Returns a list of J strings that evaluate Sum_ik nu_ik func_ik for
        each reaction.
        """
        func_list = []
        for j in range(self.J):
            func_j = []
            reactants = self.reactions[j]['reactants']
            products = self.reactions[j]['products']
            members = reactants + products
            for m in range(len(members)):
                # Append to list of non-zero components of \Sum\nu_i^k func_i^k
                func_str = "{nu_ik}*(*_phases[{i}]).{func}(T, P, _C[{i}])[{k}]"
                func_j.append(func_str.format(nu_ik=-self._nu[j][m],
                                              func=func,
                                              i=self._indices[j][m][0],
                                              k=self._indices[j][m][1]))
            func_list.append(" + ".join(func_j))

        return func_list

    def sum_nu_var_list(self, var='_Mu'):
        """Returns a list of J strings that evaluate Sum_ik nu_ik M_ik for
        each reaction.
        """
        func_list = []
        for j in range(self.J):
            func_j = []
            reactants = self.reactions[j]['reactants']
            products = self.reactions[j]['products']
            members = reactants + products
            for m in range(len(members)):
                # Append to list of non-zero components of \Sum\nu_i^k func_i^k
                func_str = "{nu_ik}*{var}[{i}][{k}]"
                func_j.append(func_str.format(nu_ik=-self._nu[j][m],
                                              var=var,
                                              i=self._indices[j][m][0],
                                              k=self._indices[j][m][1]))
            func_list.append(" + ".join(func_j))

        return func_list


    def Affinity(self):
        """Format the affinity."""
        A_list = self.sum_nu_var_list('_Mu')
        A_template = ",\n  ".join(A_list)

        return A_template


    def dA_dT(self):
        """Format the derivative of the affinity w.r.t. T."""
        dA_dT_list = self.sum_nu_var_list('_dMu_dT')
        dA_dT_template = ",\n  ".join(dA_dT_list)

        return dA_dT_template

    def dA_dP(self):
        """Format the derivative of the affinity w.r.t. P."""
        dA_dP_list = self.sum_nu_var_list('_dMu_dP')
        dA_dP_template = ",\n  ".join(dA_dP_list)

        return dA_dP_template

    def Gamma_i_expr(self):
        """
        Generate a list of SymPy expressions for mass transfer rates per phase.

        This function returns a list of N expressions for the mass transfer
        rates Gamma_i = Sum_jk nu_m_jk R_j, where R_j is the rate for reaction
        j and n_m_jk is the mass weighted stoichiometric coefficient of
        component k (in phase i), for reaction j.

        Returns
        -------
        Gamma_list : list of SymPy expressions

        """
        # List of reaction rates R = [R[0],...,R[J-1]] for J reactions
        rdict = self.model_dict['reactions']
        R = [rdict[j]['rate']['expression'] for j in range(self.J)]

        # List of mass transfer rates Gamma = [Gamma[0],...,Gamma[N-1]]
        Gamma_list = []
        nu_m = self.set_nu_m_matrix()

        for i in range(self.N):
            Gamma_i = 0.
            for j in range(self.J):
                Gamma_i += sum(nu_m[j][i])*R[j]
            Gamma_list.append(Gamma_i)

        return Gamma_list

    def Gamma_ik_expr(self):
        """
        Generate a list of lists of SymPy expressions for mass transfer rates
        per component.

        This function returns a ragged array of N x K_i (i=0,...,N) expressions
        that that evaluates Gamma^k_i = Sum_j nu_m_jk R_j for each endmember
        in each phase.

        Returns
        -------
        Gamma_list : ragged array SymPy expressions

        """
        rdict = self.model_dict['reactions']
        R = [rdict[j]['rate']['expression'] for j in range(self.J)]

        Gamma_list = []
        nu_m = self.set_nu_m_matrix()

        for i, phase in enumerate(self.phases):
            Gamma_i_list = []
            for k in range(len(phase['endmembers'])):
                Gamma_ik = 0.
                for j in range(self.J):
                    Gamma_ik += nu_m[j][i][k]*R[j]
                Gamma_i_list.append(Gamma_ik)
            Gamma_list.append(Gamma_i_list)

        return Gamma_list

    def Gamma_i(self):
        """Format the mass transfer rate for ith phase."""
        Gamma_template = """double Gamma_i;\n\n"""
        Gamma_template += """  switch(i) {\n"""
        for i in range(self.N):
            Gamma_i_expr = self._Gamma_phases[i]
            Gamma_i_str = """    case {i}: Gamma_i = {Gamma_i}; break;\n"""
            Gamma_template += Gamma_i_str.format(
                i=i, Gamma_i=self.printer.doprint(Gamma_i_expr))

        Gamma_template += """  }\n"""
        Gamma_template += """  return Gamma_i;"""

        return Gamma_template

    def dGamma_phases_dT(self):
        """List of SymPy expressions for the derivative of Gamma_i w.r.t T."""
        _dGamma_dT = [x.diff(self.T) for x in self._Gamma_phases]

        return _dGamma_dT

    def dGamma_i_dT(self):
        """Format dGamma_i_dT for the ith phase."""
        dGamma_dT_template = """double dGamma_i_dT;\n\n"""
        dGamma_dT_template += """  switch(i) {\n"""
        _dGamma_phases_dT = self.dGamma_phases_dT()

        for i in range(self.N):
            dGamma_dT_str = """    case {i}: dGamma_i_dT = {dGamma_i_dT}; """
            dGamma_dT_str += """break;\n"""
            dGamma_dT_template += dGamma_dT_str.format(
                i=i, dGamma_i_dT=self.printer.doprint(_dGamma_phases_dT[i]))

        dGamma_dT_template += """  }\n"""
        dGamma_dT_template += """  return dGamma_i_dT;"""

        return dGamma_dT_template

    def dGamma_phases_dP(self):
        """List of SymPy expressions for the derivative of Gamma_i w.r.t P."""
        _dGamma_dP = [x.diff(self.P) for x in self._Gamma_phases]

        return _dGamma_dP

    def dGamma_i_dP(self):
        """Format dGamma_i_dP for the ith phase."""
        dGamma_dP_template = """double dGamma_i_dP;\n\n"""
        dGamma_dP_template += """  switch(i) {\n"""
        _dGamma_phases_dP = self.dGamma_phases_dP()

        for i in range(self.N):
            dGamma_dP_str = """    case {i}: dGamma_i_dP = {dGamma_i_dP}; """
            dGamma_dP_str += """break;\n"""
            dGamma_dP_template += dGamma_dP_str.format(
                i=i, dGamma_i_dP=self.printer.doprint(_dGamma_phases_dP[i]))

        dGamma_dP_template += """  }\n"""
        dGamma_dP_template += """  return dGamma_i_dP;"""

        return dGamma_dP_template

    def dGamma_phases_dPhi(self):
        """Returns full formatted derivative of Gamma w.r.t. to vector of phase
        fractions Phi.
        """
        dGamma_dPhi_list = []
        for j in range(self.N):
            _phases = []
            for i, phase in enumerate(self.phases):
                zero_dict = {sym.Derivative(self.C, self.Phi[i, 0]): 0.
                             for i in range(self.N)}
                dPhi = self._Gamma_phases[j].diff(self.Phi[i]).subs(zero_dict)
                _phases.append(self.printer.doprint(dPhi))

            dGamma_dPhi_list.append("{" + ", ".join(_phases) + "}")
        dGamma_dPhi_template = (",\n  ".join(dGamma_dPhi_list))

        return dGamma_dPhi_template

    def dGamma_i_dPhi(self):
        """Returns the formatted component of the derivative of Gamma_i with
        respect to the lth phase fraction Phi_l.
        """
        dGamma_dPhi_template = """unsigned m = i*{} + l;\n""".format(self.N)
        dGamma_dPhi_template += """  double dGamma_i_dPhi;"""
        dGamma_dPhi_template += """\n\n  switch(m) {\n"""

        for j in range(self.N):
            for i, phase in enumerate(self.phases):
                m = j*self.N + i
                zero_dict = {sym.Derivative(self.C, self.Phi[i, 0]): 0.
                             for i in range(self.N)}
                dPhi = self._Gamma_phases[j].diff(self.Phi[i]).subs(zero_dict)
                dPhi = self.printer.doprint(dPhi)

                dGamma_dPhi_str = """    case {m}: dGamma_i_dPhi = {dPhi}; """
                dGamma_dPhi_str += """break;\n"""
                dGamma_dPhi_template += dGamma_dPhi_str.format(m=m, dPhi=dPhi)

        dGamma_dPhi_template += """  }\n"""
        dGamma_dPhi_template += """  return dGamma_i_dPhi;"""

        return dGamma_dPhi_template

    def dGamma_phases_dC(self):
        """Returns the full formatted derivative of Gamma w.r.t to
        concentration matrix C.
        """
        dGamma_dC_list = []
        for j in range(self.N):
            _phases = []
            for i, phase in enumerate(self.phases):
                _endmembers = []
                for k in range(len(phase['endmembers'])):
                    dA_dC = self._dA_dC
                    dC = self._Gamma_phases[j].diff(self.C[i, k]).subs(dA_dC)
                    _endmembers.append(self.printer.doprint(dC))

                _phases.append("{" + ", ".join(_endmembers) + "}")
            dGamma_dC_list.append("{" + ",\n   ".join(_phases) + "}")
        dGamma_dC_template = (",\n  ".join(dGamma_dC_list))

        return dGamma_dC_template

    def dGamma_i_dC(self):
        """Returns the formatted component of the compositional jacobian
        of Gamma_i with respect to C_l^k.
        """
        dGamma_dC_str = """unsigned m = i*{KN} + l*{K} + k;\n"""
        dGamma_dC_template = dGamma_dC_str.format(KN=self.Kmax*self.N,
                                                  K=self.Kmax)
        dGamma_dC_template += """  double dGamma_i_dC;"""
        dGamma_dC_template += """\n\n  switch(m) {\n"""

        # Generate a list of SymPy functions for the affinities
        A = [sym.Function('A')(self.T, self.P, self.C, j)
             for j in range(self.J)]

        for i in range(self.N):
            for l, phase in enumerate(self.phases):
                for k in range(len(phase['endmembers'])):
                    m = i*self.N*self.Kmax + l*self.Kmax + k
                    dA_dC = self._dA_dC
                    dC = self._Gamma_phases[i].diff(self.C[l, k]).subs(dA_dC)
                    dC = self.printer.doprint(dC)

                    dGamma_dC_str = """    case {m}: dGamma_i_dC = {dC}; """
                    dGamma_dC_str += """break;\n"""
                    dGamma_dC_template += dGamma_dC_str.format(m=m, dC=dC)

        dGamma_dC_template += """  }\n"""
        dGamma_dC_template += """  return dGamma_i_dC;"""

        return dGamma_dC_template

    def Gamma_components(self):
        """Format vector of vectors of mass transfer rates (per component
        in each phase).
        """
        Gamma_list = []
        for i, phase in enumerate(self.phases):
            _endmembers = []
            for k in range(len(phase['endmembers'])):
                Gamma_ik = self.printer.doprint(self._Gamma_components[i][k])
                _endmembers.append(Gamma_ik)

            Gamma_list.append("{" + ", ".join(_endmembers) + "}")
        Gamma_template = (",\n  ".join(Gamma_list))

        return Gamma_template

    def Gamma_ik(self):
        """Returns the formatted mass transfer rate of component k in phase i.
        """
        Gamma_ik_template = """unsigned m = i*{K} + k;\n""".format(K=self.Kmax)
        Gamma_ik_template += """  double Gamma_ik;"""
        Gamma_ik_template += """\n\n  switch(m) {\n"""

        # Loop over phases and endmembers
        for i, phase in enumerate(self.phases):
            for k in range(len(phase['endmembers'])):
                m = i*self.Kmax + k
                Gamma_ik = self.printer.doprint(self._Gamma_components[i][k])
                Gamma_ik_str = """    case {m}: Gamma_ik = {G}; break;\n"""
                Gamma_ik_template += Gamma_ik_str.format(m=m, G=Gamma_ik)

        Gamma_ik_template += """  }\n"""
        Gamma_ik_template += """  return Gamma_ik;"""

        return Gamma_ik_template

    def dGamma_components_dT(self):
        """Format vector of vectors of mass transfer rates (per component
        in each phase) differentiated w.r.t T.
        """
        dGamma_dT_list = []
        for i, phase in enumerate(self.phases):
            _endmembers = []
            for k in range(len(phase['endmembers'])):
                dGamma_ik_dT = self._Gamma_components[i][k].diff(self.T)
                dGamma_ik_dT = self.printer.doprint(dGamma_ik_dT)
                _endmembers.append(dGamma_ik_dT)

            dGamma_dT_list.append("{" + ", ".join(_endmembers) + "}")
        dGamma_dT_template = (",\n  ".join(dGamma_dT_list))

        return dGamma_dT_template

    def dGamma_ik_dT(self):
        """Returns the formatted mass transfer rate of component k in phase i
        differentiated w.r.t T.
        """
        dGamma_ik_dT_str = """unsigned m = i*{K} + k;\n"""
        dGamma_ik_dT_template = dGamma_ik_dT_str.format(K=self.Kmax)
        dGamma_ik_dT_template += """  double dGamma_ik_dT;"""
        dGamma_ik_dT_template += """\n\n  switch(m) {\n"""

        # Loop over phases and endmembers
        for i, phase in enumerate(self.phases):
            for k in range(len(phase['endmembers'])):
                m = i*self.Kmax + k
                dGamma_ik_dT = self._Gamma_components[i][k].diff(self.T)
                dGamma_ik_dT = self.printer.doprint(dGamma_ik_dT)
                dGamma_ik_dT_str = """    case {m}: dGamma_ik_dT = {G}; """
                dGamma_ik_dT_str += """break;\n"""
                dGamma_ik_dT_template += dGamma_ik_dT_str.format(
                    m=m, G=dGamma_ik_dT)

        dGamma_ik_dT_template += """  }\n"""
        dGamma_ik_dT_template += """  return dGamma_ik_dT;"""

        return dGamma_ik_dT_template

    def dGamma_components_dP(self):
        """Format vector of vectors of mass transfer rates (per component
        in each phase) differentiated w.r.t P.
        """
        dGamma_dP_list = []
        for i, phase in enumerate(self.phases):
            _endmembers = []
            for k in range(len(phase['endmembers'])):
                dGamma_ik_dP = self._Gamma_components[i][k].diff(self.P)
                dGamma_ik_dP = self.printer.doprint(dGamma_ik_dP)
                _endmembers.append(dGamma_ik_dP)

            dGamma_dP_list.append("{" + ", ".join(_endmembers) + "}")
        dGamma_dP_template = (",\n  ".join(dGamma_dP_list))

        return dGamma_dP_template

    def dGamma_ik_dP(self):
        """Returns the formatted mass transfer rate of component k in phase i
        differentiated w.r.t P.
        """
        dGamma_ik_dP_str = """unsigned m = i*{K} + k;\n"""
        dGamma_ik_dP_template = dGamma_ik_dP_str.format(K=self.Kmax)
        dGamma_ik_dP_template += """  double dGamma_ik_dP;"""
        dGamma_ik_dP_template += """\n\n  switch(m) {\n"""

        # Loop over phases and endmembers
        for i, phase in enumerate(self.phases):
            for k in range(len(phase['endmembers'])):
                m = i*self.Kmax + k
                dGamma_ik_dP = self._Gamma_components[i][k].diff(self.P)
                dGamma_ik_dP = self.printer.doprint(dGamma_ik_dP)
                dGamma_ik_dP_str = """    case {m}: dGamma_ik_dP = {G}; """
                dGamma_ik_dP_str += """break;\n"""
                dGamma_ik_dP_template += dGamma_ik_dP_str.format(
                    m=m, G=dGamma_ik_dP)

        dGamma_ik_dP_template += """  }\n"""
        dGamma_ik_dP_template += """  return dGamma_ik_dP;"""

        return dGamma_ik_dP_template

    def dGamma_ik_dPhi(self):
        """Returns the formatted component of the derivative of Gamma_i^k
        w.r.t. Phi_l.
        """
        dGamma_dPhi_str = """unsigned m = l*{KN} + i*{K} + k;\n"""
        dGamma_dPhi_template = dGamma_dPhi_str.format(KN=self.Kmax*self.N,
                                                      K=self.Kmax)
        dGamma_dPhi_template += """  double dGamma_ik_dPhi;"""
        dGamma_dPhi_template += """\n\n  switch(m) {\n"""

        for l in range(self.N):
            for i, phase in enumerate(self.phases):
                for k in range(len(phase['endmembers'])):
                    m = l*self.N*self.Kmax + i*self.Kmax + k
                    zero_dict = {sym.Derivative(self.C, self.Phi[j, 0]): 0.
                                 for j in range(self.N)}
                    dPhi = self._Gamma_components[i][k].diff(self.Phi[l])
                    dPhi = dPhi.subs(zero_dict)
                    dPhi = self.printer.doprint(dPhi)
                    dGamma_dPhi_str = """    case {m}: dGamma_ik_dPhi """
                    dGamma_dPhi_str += """= {dPhi}; break;\n"""
                    dGamma_dPhi_template += dGamma_dPhi_str.format(
                        m=m, dPhi=dPhi)

        dGamma_dPhi_template += """  }\n"""
        dGamma_dPhi_template += """  return dGamma_ik_dPhi;"""

        return dGamma_dPhi_template

    def dGamma_ik_dC_i(self):
        """Returns the  compositional jacobian of Gamma_i^k with respect to the concentration of phase i
        (mainly useful for solution phases, otherwise 0).
        """

        dGamma_dC_template = """  switch(i) {\n"""

        # Generate a list of SymPy functions for the affinities
        A = [sym.Function('A')(self.T, self.P, self.C, j)
             for j in range(self.J)]

        for i, phase_i in enumerate(self.phases):
            dGamma_dC_template += """    case {i}: _dGamma = {{\n """.format(i=i)
            K = len(phase_i['endmembers'])
            dCi_list = []
            for k in range(K):
                # Generate SymPy expressions for dGamma_i^k_dC_i
                dC_list = []
                for m in range(K):
                    dC = self._Gamma_components[i][k].diff(self.C[i, m])
                    dC = dC.subs(self._dA_dC)
                    dC_list.append(self.printer.doprint(dC))
                dCi_list.append("{" + ",\n ".join(dC_list) + "}")
            dGamma_dC_template += ",\n        ".join(dCi_list) + "\n    }; break;\n\n"
        dGamma_dC_template += """  }\n"""

        return dGamma_dC_template
    
    def dGamma_ik_dPhi_i(self):
        """Returns the  compositional jacobian of Gamma_i^k with respect to the concentration of phase i
        (mainly useful for solution phases, otherwise 0).
        """

        dGamma_dPhi_template = """  switch(i) {\n"""

        # Generate a list of SymPy functions for the affinities
        A = [sym.Function('A')(self.T, self.P, self.C, j)
             for j in range(self.J)]
        # wierd need to zero out derivatives of C with respect to Phi (
        zero_dict = {sym.Derivative(self.C, self.Phi[i, 0]): 0.
                     for i in range(self.N)}

        for i, phase_i in enumerate(self.phases):
            dGamma_dPhi_template += """    case {i}: _dGamma = {{\n """.format(i=i)
            K = len(phase_i['endmembers'])
            dPhii_list = []
            for k in range(K):
                # Generate SymPy expressions for dGamma_i^k_dC_i
                dPhi_list = []
                for m in range(self.N):
                    dPhi = self._Gamma_components[i][k].diff(self.Phi[m]).subs(zero_dict)
                    dPhi_list.append(self.printer.doprint(dPhi))
                dPhii_list.append("{" + ",\n ".join(dPhi_list) + "}")
            dGamma_dPhi_template += ",\n        ".join(dPhii_list) + "\n    }; break;\n\n"
        dGamma_dPhi_template += """  }\n"""

        return dGamma_dPhi_template


    def dGamma_ik_dC(self):
        """Returns the formatted component of the compositional jacobian
        of Gamma_i^k with respect to C_l^m.
        """
        dGamma_dC_str = """unsigned p = l*{K2N} + m*{KN} + i*{K}+ k;\n"""
        dGamma_dC_template = dGamma_dC_str.format(K2N=self.Kmax**2*self.N,
                                                  KN=self.Kmax*self.N,
                                                  K=self.Kmax)
        dGamma_dC_template += """  double dGamma_ik_dC;"""
        dGamma_dC_template += """\n\n  switch(p) {\n"""

        # Generate a list of SymPy functions for the affinities
        A = [sym.Function('A')(self.T, self.P, self.C, j)
             for j in range(self.J)]

        for l, phase_l in enumerate(self.phases):
            for m in range(len(phase_l['endmembers'])):
                for i, phase_i in enumerate(self.phases):
                    for k in range(len(phase_i['endmembers'])):
                        p = l*self.N*self.Kmax**2 + m*self.Kmax*self.N
                        p += i*self.Kmax + k

                        # Generate SymPy expression for dGamma_i^k_dC_l^m
                        dC = self._Gamma_components[i][k].diff(self.C[l, m])
                        dC = dC.subs(self._dA_dC)

                        # Send dGamma_i^k_dC_l^m to code printer and substitute
                        dC = self.printer.doprint(dC)
                        dGamma_dC_str = """    case {p}: dGamma_ik_dC = """
                        dGamma_dC_str += """{dC}; break;\n"""
                        dGamma_dC_template += dGamma_dC_str.format(p=p, dC=dC)

        dGamma_dC_template += """  }\n"""
        dGamma_dC_template += """  return dGamma_ik_dC;"""

        return dGamma_dC_template

    def M_products(self):
        """Calculates total mass of products for each reaction."""
        M_products = []

        # Loop over all reactions
        for j in range(self.J):
            M_j = []
            for k in np.where(self._nu[j] > 0)[0]:
                M_j.append("""{nu}*(*_phases[{j}]).get_M()[{k}]""".format(
                                nu=self._nu[j][k],
                                j=self._indices[j][k][0],
                                k=self._indices[j][k][1]))

            M_products.append("(" + " + ".join(M_j) + ")")

        return M_products

    def vector_functions(self):
        """Dictionary of code printed vector functions."""
        vector_funcs = {"Gamma_phases": self._Gamma_phases,
                        "dGamma_phases_dT": self.dGamma_phases_dT(),
                        "dGamma_phases_dP": self.dGamma_phases_dP()}

        # Code print functions
        def f(x): return """{f_i}""".format(f_i=self.printer.doprint(x))
        vector_funcs.update({k: ',\n  '.join(list(map(f, v)))
                             for k, v in vector_funcs.items()})

        return vector_funcs
