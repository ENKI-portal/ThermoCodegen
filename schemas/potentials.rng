<?xml version="1.0" encoding="UTF-8"?>
<grammar xmlns:a="http://relaxng.org/ns/compatibility/annotations/1.0" xmlns="http://relaxng.org/ns/structure/1.0" datatypeLibrary="http://www.w3.org/2001/XMLSchema-datatypes">
  <!--
    Copyright (C) 2018 Columbia University in the City of New York and others.
    
    Please see the AUTHORS file in the main source directory for a full list
    of contributors.
    
    This file is part of enki-codegen.
    
    enki-codegen is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.
    
    enki-codegen is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU Lesser General Public License for more details.
    
    You should have received a copy of the GNU Lesser General Public License
    along with enki-codegen. If not, see <http://www.gnu.org/licenses/>.
    
  -->
  <include href="spud_base.rng"/>
  <include href="variable.rng"/>
  <include href="parameter.rng"/>
  <include href="function.rng"/>
  <define name="generic_free_energy_model">
    <element name="free_energy_model">
      <a:documentation>Free Energy model:  sets global variables (e.g. T,P for Gibbs Free energy, T,V, for Helmholtz)</a:documentation>
      <attribute name="name">
        <data type="string"/>
      </attribute>
      <oneOrMore>
        <ref name="generic_variable"/>
      </oneOrMore>
      <oneOrMore>
        <ref name="generic_parameter"/>
      </oneOrMore>
      <ref name="comment"/>
    </element>
  </define>
  <define name="gibbs_free_energy_model">
    <element name="free_energy_model">
      <a:documentation>Gibbs Free Energy model:  G(T,P)</a:documentation>
      <attribute name="name">
        <value>Gibbs</value>
      </attribute>
      <ref name="variable_T"/>
      <ref name="variable_P"/>
      <ref name="parameter_Tr"/>
      <ref name="parameter_Pr"/>
      <ref name="comment"/>
    </element>
  </define>
  <define name="helmholtz_free_energy_model">
    <element name="free_energy_model">
      <a:documentation>Helmholtz Free Energy model:  A(T,V)</a:documentation>
      <attribute name="name">
        <value>Helmholtz</value>
      </attribute>
      <ref name="variable_T"/>
      <ref name="variable_V"/>
      <ref name="parameter_Tr"/>
      <ref name="parameter_Vr"/>
      <ref name="comment"/>
    </element>
  </define>
  <define name="phase_gibbs_free_energy_model">
    <element name="free_energy_model">
      <a:documentation>Gibbs Free Energy model:  G(T,P,n)</a:documentation>
      <attribute name="name">
        <value>Gibbs</value>
      </attribute>
      <ref name="variable_T"/>
      <ref name="variable_P"/>
      <ref name="variable_n"/>
      <ref name="function_mu_G"/>
      <ref name="comment"/>
    </element>
  </define>
  <define name="phase_helmholtz_free_energy_model">
    <element name="free_energy_model">
      <a:documentation>Helmholtz Free Energy model:  A(T,V,n)</a:documentation>
      <attribute name="name">
        <value>Helmholtz</value>
      </attribute>
      <ref name="variable_T"/>
      <ref name="variable_V"/>
      <ref name="variable_n"/>
      <ref name="function_mu_A"/>
      <ref name="comment"/>
    </element>
  </define>
  <define name="reference">
    <element name="reference">
      <a:documentation> string to hold reference information (url, doi, SHA) for</a:documentation>
      <ref name="anystring"/>
      <ref name="comment"/>
    </element>
  </define>
  <define name="expression">
    <element name="expression">
      <a:documentation>Expression:  sympy expression, assumes sympy namespace as sym</a:documentation>
      <ref name="python_code"/>
    </element>
  </define>
  <define name="residual">
    <element name="residual">
      <a:documentation>Residual:  sympy expression F, such that F(u)=0, assumes sympy namespace as sym,  </a:documentation>
      <attribute name="name">
        <value>F</value>
      </attribute>
      <ref name="python_code"/>
    </element>
  </define>
  <define name="generic_potential">
    <element name="potential">
      <attribute name="name">
        <data type="string"/>
      </attribute>
      <zeroOrMore>
        <ref name="generic_parameter"/>
      </zeroOrMore>
      <zeroOrMore>
        <ref name="generic_function"/>
      </zeroOrMore>
      <ref name="expression"/>
      <optional>
        <ref name="reference"/>
      </optional>
      <ref name="comment"/>
    </element>
  </define>
</grammar>
