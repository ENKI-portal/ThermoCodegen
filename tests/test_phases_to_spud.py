import filecmp
import os
import shutil
from glob import glob
import subprocess

def test_phases_to_phml():
	tcg_home = os.environ['THERMOCODEGEN_HOME']
	path = os.path.abspath(tcg_home+'/share/thermocodegen/examples/Notebooks/coder_to_xml/phases')
	notebooks = glob(path+'/Example*.ipynb')
	for notebook in notebooks:
		subprocess.run(['jupyter', 'nbconvert', '--to', 'notebook', '--execute', notebook] )
	spuddirs = glob(path+'/spudfiles_*')
	try:
		shutil.rmtree('tmp')
	except:
		pass
	tmp_path = 'tmp/phase_spudfiles'
	os.makedirs(tmp_path, exist_ok=True)
	for _dir in spuddirs:
		print('moving {}'.format(os.path.basename(_dir)))
		shutil.move(_dir, tmp_path)

def test_phmls():
	phmls = glob('data/phase_spudfiles/*/*.phml')
	for file in phmls:
		test_file = file.replace('data', 'tmp')
		comp = filecmp.cmp(test_file, file)
		if not comp:
			print('Error:  mismatch in file {}'.format(os.path.basename(file)))
			assert filecmp.cmp(dev_file, file)

	print('Success! all files compare true')



if __name__ == "__main__":
	test_phases_to_phml()
	test_phmls()