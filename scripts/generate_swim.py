# -*- coding: utf-8 -*-
"Script to generate SWIM related code"

import sys
import os
import subprocess
import warnings
import time
from thermocodegen.spudio import endmember
from generate_endmember import set_identifier
from string import Template as template

warnings.simplefilter('ignore')


def main():
    # Path to templates
    tcg_home = os.environ.get('THERMOCODEGEN_HOME')
    if tcg_home is None:
        raise OSError('Environment variable THERMOCODEGEN_HOME is not set')

    coder_dir = tcg_home + '/share/thermocodegen/templates/coder'
    template_dir = tcg_home + '/share/thermocodegen/templates/cpp'

    # Get timestamp for identifier string
    identifier = time.asctime(time.localtime(time.time()))

    # Path to build directory
    build_dir = sys.argv[1]

    # Create directory to put coder generated C source
    try:
        coder_src_dir = '{}/src/coder'.format(build_dir)
        os.makedirs(coder_src_dir)
    except OSError:
        pass

    os.chdir(coder_src_dir)

    # Load header template
    with open(coder_dir + "/SWIM_water_coder.h", 'r') as _file:
        c_header = _file.read()

    # Load source template
    with open(coder_dir + "/SWIM_water_coder.c", 'r') as _file:
        c_source = _file.read()

    c_source = template(c_source).safe_substitute(dict(identifier=identifier))

    # Write header file
    with open(coder_src_dir + "/SWIM_water_coder.h", "w") as header_file:
        header_file.write(c_header)

    # Write source file
    with open(coder_src_dir + "/SWIM_water_coder.c", "w") as source_file:
        source_file.write(c_source)

    os.chdir(build_dir)

    # Make sure that directories exist
    try:
        os.makedirs('{}/include/endmembers'.format(build_dir))
    except FileExistsError:
        pass
    try:
        os.makedirs('{}/src/endmembers'.format(build_dir))
    except FileExistsError:
        pass

    # Touch include file if it doesn't exist
    open(build_dir + "/include/" + "endmembers.h", 'a').close()

    # Check if endmember needs to be included
    write_to_include = True
    with open(build_dir + "/include/" + "endmembers.h") as include_file:
        line = include_file.readlines()
        if '#include \"endmembers/SWIM_water.h\"\n'in line:
            write_to_include = False

    # Add endmember to include file
    if write_to_include:
        include_file = open(build_dir + "/include/" + "endmembers.h", "a")
        include_file.write('#include \"endmembers/SWIM_water.h\"\n')
        include_file.close()

    # Add include and src dirs
    include_dir = build_dir + "/include/endmembers/"
    src_dir = build_dir + "/src/endmembers/"

    # Load header template
    with open(template_dir + '/endmembers/endmember.h', 'r') as _file:
        header = _file.read()

    # Load source template
    with open(template_dir + '/endmembers/endmember.cpp', 'r') as _file:
        source = _file.read()

    ndict = dict(endmembername="SWIM_water",
                 C_name="SWIM_water_coder",
                 endmembername_uppercase="SWIM_WATER")
    header = template(header).safe_substitute(ndict)
    source = template(source).safe_substitute(ndict)

    # Write header file
    with open(include_dir + "SWIM_water.h", "w") as header_file:
        header_file.write(header)

    # Write source file
    with open(src_dir + "SWIM_water.cpp", "w") as source_file:
        source_file.write(source)


if __name__ == '__main__':
    main()
