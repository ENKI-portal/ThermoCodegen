.. _tcg_builddb:

tcg_builddb
===========

.. argparse::
    :filename: ../bin/tcg_builddb
    :func: tcg_builddb_parser
    :prog: tcg_builddb
