Quickstart
===================================

The easiest way to get started with ThermoCodegen is to install
`docker <https://www.docker.com>`_ or `singularity <https://sylabs.io/singularity>`_
and use one of our pre-compiled containers.

We recommend `Docker Desktop
<https://docs.docker.com/get-docker/>`_ on Macs, and `singularity
<https://sylabs.io/singularity>`_  on
linux machines.

For complete functionality you will also need an XServer.  On Macs we
use `XQuartz <https://www.xquartz.org>`_.  Thermocodegen has not
been tested on Windows machines but we expect docker to work there as
well. Any assistance with testing on Windows boxes is greatly appreciated.


.. toctree::
   :maxdepth: 1
   :caption: Containers

   docker
   singularity
