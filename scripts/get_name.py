#!/usr/bin/env python3
#  fast libspud version to parse name from input files
import sys
import libspud

filename = sys.argv[1]

# note: this will die if filename is not a valid spud name (i.e. .emml, .phml or .rxml file)
libspud.load_options(filename)
name = libspud.get_option('/name/name')
print(name, end='')
libspud.clear_options()
