# Copyright (C) 2018 Columbia University in the City of New York and others.
#
# Please see the AUTHORS file in the main source directory for a full list
# of contributors.
#
# This file is part of enki-codegen.
#
# enki-codegen is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# enki-codegen is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with enki-codegen. If not, see <http://www.gnu.org/licenses/>.
#


include "spud_base.rnc"

generic_symbol =
(
	element symbol {
			anystring
	}
)

scalar =
(
## The rank of the variable (scalar, rank 0).
            element rank {
              attribute name { "Scalar" }
            }
)

vector =
(
## The rank of the variable (vector, rank 1).
            element rank {
              attribute name { "Vector" },
	      ## length of vector (for checking shape)
	      element size {
	      	      integer
	      }
            }
)

matrix =
(
## The rank of the variable (matrix, rank 2).
        element rank {
        attribute name { "Matrix" },
	      	## number of rows
	      	element n_rows {
	      		integer
	      	},
					## number of columns
					element n_cols {
	      		integer
	      	}
				}
)


vector_K =
(
## The rank of the variable (vector, rank 1).
            	element rank {
              		attribute name { "Vector" },
	      		## length of vector (set by number of end-members)
	      		element size {
	      	      		"K"
	      		}
            	}
)

vector_N =
(
## The rank of the variable (vector, rank 1).
            	element rank {
              		attribute name { "Vector" },
	      		## length of vector (set by number of phases)
	      		element size {
	      	      		"N"
	      		}
            	}
)

vector_J =
(
## The rank of the variable (vector, rank 1).
	element rank {
    	attribute name { "Vector" },
	    ## length of vector (set by number of reactions)
	    element size {
	     	"J"
	    }
   	}
)

matrix_N_by_Kmax =
(
## The rank of the variable (matrix, rank 2).
	element rank {
    	attribute name { "Matrix" },
		## number of rows set by maximum number of
		## phases N
	    element n_rows {
	      	    "N"
	    },
		## number of columns set by maximum number of
		## endmembers Kmax
		element n_cols {
	      	   "Kmax"
	    }
    }
)

generic_variable =
  (
	## Model Variable, these are independent variables of the thermodynamic functions:  Every parameter has a name, an optional symbol and  units.
	element variable {
		attribute name { xsd:string},
	    	( scalar | vector | matrix),
		## space separated list of strings of units
		element units {
	      	    attribute lines { "1" },
	      	    list { xsd:string+ }
		},
		##  Optional sympy symbol for use with this variable, otherwise will default to the variable name
		## string describing the units of the variable
		generic_symbol?,
        comment
    	}
  )


variable_T =
(
	## Model Variable, these are independent variables of the thermodynamic functions
	## Temperature in Kelvin
	element variable {
		attribute name { 'T' },
		scalar,
		## string describing the units of the variable
		element  units {   	      'K'	},
        	comment
	}
  )

variable_P =
(
	## Model Variable, these are independent variables of the thermodynamic functions
	## Pressure in bars
	element variable {
		attribute name { 'P' },
		scalar,
		## string describing the units of the variable
		element  units {
	      	      'bar'
		},
        	comment
	}
  )

variable_V =
(
	## Model Variable, these are independent variables of the thermodynamic functions
	##
	## Volume in bars
	element variable {
		attribute name { 'V' },
		scalar,
		## string describing the units of the variable
		element  units {
	      	      'J/bar-m'
		},
        	comment
	}
  )


variable_n =
(
	## Model Variable, these are independent variables of the thermodynamic functions
	##
	## vector of moles of components.  The size is automatically set by the spud parser
	## depending on the number of endmembers included in the file
	element variable {
		attribute name { 'n' },
		vector_K,
		## string describing the units of the variable
		element  units {
	      	      'mol'
		},
		generic_symbol?,
        	comment
	}
  )

variable_s =
(
	## optional vector of order parameters s.  Size must be set by the model
	element variable {
		attribute name { xsd:string },
		vector,
		## string describing the units of the variable
		element  units {
	      	      'None'
		},
		generic_symbol?,
        comment
	}
)

variable_C =
(
	## Model Variable, these are independent variables of the thermodynamic functions
	##
	## matrix of mass weighted concentrations of components in phases.  The size is automatically set by the spud parser
	## depending on the number of endmembers and phases included in the file
	element variable {
		attribute name { 'C' },
		matrix_N_by_Kmax,
		element  units {
	      	      'None'
		},
			    comment
	}
	)

variable_Phi =
(
	## Model Variable, these are independent variables of the thermodynamic functions
	##
	## vector of phase volume fractions.  The size is automatically set by the spud parser
	## depending on the number of phases included in the file
	element variable {
		attribute name { 'Phi' },
		vector_N,
		## string describing the units of the variable
		element  units {
		     	      '%'
		},
	        comment
	}
	)
