#!/usr/bin/env python3

"""
   parsers:

    collection of python utilities for parsing and writing specific options tree structures
    from Spud xml files
    and returning python for use in general code-generation

"""

# import basic Parameter and expression classes from Coder
# defer this, let's do everything here as a pure python dictionary
# and translate what's needed to coder.
#from   thermocodegen.coder.coder import Parameter, Expression
from collections import Counter

# import libspud for reading SPuD xml files
import libspud

# import sympy for symbolic preprocessing
import sympy as sym
import textwrap
import re


# Parsers


# convenience function for returning list of names from lists of dictionaries
get_names = lambda list : [ list[i]['name'] for i in range(len(list))]
get_variables = lambda dict: [ variable['symbol'] for variable in dict['model']['variables']]


def parse_name(file):
    """reads a SPud derived  description file and just extracts the name"""
    libspud.load_options(file)
    name = libspud.get_option('/name/name')
    libspud.clear_options()
    return name


def parse_all_parameters(file):
    """Reads a SPuD derived  description file (.emml, .phml or .rxml file)
    and returns a list of all  parameters as nested dictionaries
    Also checks for unique names across all parameters
    Uses the libspud to read spud files


    Parameters
    ----------
    file: python string containing name  of an .emml file

    Output
    --------
    parameters:  python list of scalar parameter dictionaries

    Examples
    --------
    parameters = parse_all_parameters('test.emml')
    """

    # load emml files (will raise exception of no appropriate file)
    libspud.load_options(file)

    # get all keynames
    keys = [ 'free_energy_model', 'global_parameters', 'parameters' ]
    n_potentials = libspud.option_count('/potential')
    n_reactions  = libspud.option_count('/reaction')

    keys += [ 'potential[{}]'.format(i) for i in range(n_potentials)]
    keys += [ 'reaction[{}]'.format(i) for i in range(n_reactions)]

    # loop of keys and extract parameter dictionaries and separate into
    #scalar and vector valued parameters
    parameters =[]
    for key in keys:
        str = key+'/parameter'
        n_params = libspud.option_count(str)
        for i in range(n_params):
            param = parse_parameter(str+'[{}]'.format(i))
            parameters.append(param)

    libspud.clear_options()

    #check uniqueness of parameter names
    names = get_names(parameters)
    duplicates = [k for k,v in Counter(names).items() if v > 1]

    if len(duplicates) > 0:
        raise ValueError('Warning: duplicated parameter names -- {}'.format(duplicates))

    return parameters


def parse_variable(path, dict=None, symbolic=False):
    """
        returns a dictionary describing all parts of a variable
        starting at spud-path path. Returns pure Matrix Symbols
        if symbolic=True.
    """
    assert (libspud.get_option(path + '/name'))

    variable = {}
    variable['name'] = libspud.get_option(path + '/name')
    variable['rank'] = libspud.get_option(path + '/rank/name')

    if variable['rank'] == 'Vector':
        # Get vector size
        variable['size'] = libspud.get_option(path + '/rank/size')

        # If dimension is an integer then do nothing
        if (isinstance(variable['size'], int)):
            pass
        elif (isinstance(variable['size'], str)):
            # Check for string in dict keys (assumes its values is an integer)
            # FIXME: this breaks if dict-None I think,  not sure how this is meant to work
            variable['size'] = dict[variable['size']]
        else:
            raise TypeError("Size of vector must be an integer or string")

    if variable['rank'] == 'Matrix':
        # Get number of rows and columns
        n_rows = libspud.get_option(path + '/rank/n_rows')
        n_cols = libspud.get_option(path + '/rank/n_cols')
        variable['size'] = [n_rows, n_cols]

        # Type checking for rows and columns
        for k, dim in enumerate(variable['size']):
            if (isinstance(dim, int)):
                pass
            elif (isinstance(dim, str)):
                variable['size'][k] = dict[dim]
            else:
                raise TypeError("Dimension of matrix must be an integer or string")

    # Get units
    variable['units'] = libspud.get_option(path + '/units')

    # get optional symbol name or use option name
    try:
        symbol = libspud.get_option(path + '/symbol')
        variable['symbol']=sym.sympify(symbol)
    except libspud.SpudKeyError:
        symbol = variable['name']
        if variable['rank'] == 'Scalar':
            variable['symbol'] = sym.Symbol(symbol)
        elif variable['rank'] == 'Vector':
            if symbolic:
                variable['symbol'] = sym.MatrixSymbol(symbol, variable['size'], 1)
            else:
                variable['symbol'] = sym.Matrix(sym.MatrixSymbol(symbol, variable['size'], 1))
        elif variable['rank'] == 'Matrix':
            if symbolic:
                variable['symbol'] = sym.MatrixSymbol(symbol,variable['size'][0],variable['size'][1])
            else:
                variable['symbol'] = sym.Matrix(sym.MatrixSymbol(symbol, variable['size'][0], variable['size'][1]))

    return variable


def parse_parameter(path):
    """
        returns a dictionary describing all parts of a parameter
        starting at spud-path path

        input:  spud-path to parameter
        output: python dictionary with struction

        param['name'] :  string with parameter name
        param['rank'] :  'Scalar' or 'Vector'
        param['size'] :  integer length of vector (or 1 for scalar)
        param['value']:  list of floating point values
        param['units']:  list of strings for unites
        param['symbol']: sympy symbol of parameter
    """
    assert (libspud.get_option(path+'/name'))

    param = {}
    param['name']=libspud.get_option(path+'/name')
    param['rank']=libspud.get_option(path+'/rank/name')
    param['value']=libspud.get_option(path+'/rank/value')
    param['units']=libspud.get_option(path+'/rank/units')

    if param['rank'] == 'Vector':
        param['size'] = libspud.get_option(path+'/rank/size')
        # turn units into a list of strings
        param['units']=param['units'].replace('\'','').split()
    else:
        param['size'] = 1

    # get optional symbol or set to name
    try:
        symbol = libspud.get_option(path+'/symbol')
    except libspud.SpudKeyError:
        symbol = param['name']

    # set symbol to Symbol or MatrixSymbol depending on rank
    if param['rank'] == 'Scalar':
        param['symbol'] = sym.Symbol(symbol)
    else:
        param['symbol'] = sym.MatrixSymbol(symbol,param['size'],1)

    return param

def parse_function(path,dict,global_dict,symbolic=False):
    """ parses function options and returns appropriate sympy objects depending
    on whether the function is implicit or explicit

    new version tries to use Exec.

    parameters
    -----------
    path:  spud path to top of function options tree
    dict:     spud object dictionary containing variables, parameters and subs_dict
    global_dict:  global dictionary with sym namespace and parameters for exec
    local_dict: namespace dictionary of symbol definitions for use in sympify
                this dictionary is constructed and updated in parse_potential
    symbolic: If true then Matrix variables are assumed to be symbols rather than
              expressions

    returns
    --------
    dictionary of function components
    function['name']: name of function
    function['symbol'] : sympy symbol of function name
    function['type'] : explicit, implicit or external
    function['expression']: subsd in sympy expression for external functions

    if an explicit function can just set function['symbol'] = function['expression']
    otherwise set function['symbol'] =sym.Function(symbol)(Variables)
    """

    variables = dict['model']['variables']
    parameters = dict['parameters']
    subs_dict = dict['subs_dict']


    # dictionary for use in exec
    local_dict={}

    function = {}
    # get the name and set the function symbol
    name = libspud.get_option(path+'/name')
    function['name']= name

    #set function symbol and sympify or default to name
    try:
        symbol = libspud.get_option(path+'/symbol')
        function['symbol'] = sym.sympify(symbol)
        needs_symbol = False
    except libspud.SpudKeyError:
        symbol = name
        needs_symbol = True

    # get function type as explict, implicit or external
    function['type'] = libspud.get_option(path+'/type/name')
    if function['type'] == 'explicit':
        if needs_symbol:
            function['symbol'] = sym.Symbol(symbol)
        exp_str =     libspud.get_option(path+'/type/expression')
    ##FIXME: implicit functions have not be implemented properly yet
    elif function['type'] == 'implicit':
        raise NotImplentedError( "Implicit Functions are not properly implemented")
        if needs_symbol:
            function['symbol'] = sym.Function(symbol)(variables[0],variables[1])
        exp_str =     libspud.get_option(path+'/type/expression')
        ig = libspud.get_option(path+'/type/initial_guess')
        initial_guess = exec(ig,global_dict,local_dict)
        function['initial_guess'] = initial_guess.subs(subs_dict)
    elif function['type'] == 'external':
        # FIXME -- CHECKME:  These external functions are used for reactions
        exp_str = None

        # get function rank and size
        function['rank'] = libspud.get_option(path+'/type/rank/name')

        if function['rank'] == 'Vector':
            size = libspud.get_option(path+'/type/rank/size')
            function['size'] = dict[size]

        # count and collect local variables for external functions
        vpath = path+'/type/variable'
        nvar = libspud.option_count(vpath)
        ext_var = []
        for i in range(nvar):
            ext_var.append(parse_variable(vpath+'[{}]'.format(i),dict,symbolic=symbolic))

        # if needed: construct function, or vector of functions symbol
        ## FIXME:  need to recalculate symbol using exec I think
        vars = tuple([ var['symbol'] for var in ext_var ])

        # if scalar construct a function symbol(vars)
        if function['rank'] == 'Scalar':
            str = 'sym.Function(\'{}\'){}'.format(symbol,vars)
            #exec(str,global_dict,local_dict)
            if needs_symbol:
                function['symbol'] = sym.sympify(str)
        else:
            # If function is the affinity then we need to include index in variables
            _vars = [ var['symbol'] for var in ext_var ]
            if function['name'] == 'A':
                function['symbol'] = sym.Matrix([sym.Function('A')(_vars[0],_vars[1],_vars[2],i)
                                     for i in range(function['size'])])
            elif function['name'] == 'rho':
                function['symbol'] = sym.Matrix([sym.Function('rho')(_vars[0],_vars[1],i)
                                     for i in range(function['size'])])
            else:
                if needs_symbol:
                    function['symbol'] = sym.Matrix([ sym.sympify('Function(\'{}_{}\'){}'.format(symbol,i,vars))
                                    for i in range(function['size'])])

    # sympyify the expression if it exists and return expression
    if exp_str:
        exec(exp_str,global_dict,local_dict)
        expression = local_dict[function['name']]
        # subs if in any parameters not in the function list and return the sympy expression
        function['expression'] = expression.subs(subs_dict)
        global_dict[function['name']]=function['expression']

    return function

def parse_expression(path, name, global_dict):
    """
    parses an expression field of a spud file and runs the string through sympify or exec to
    return a sympy expression called name

    :param path: spud_path to beginning of expression field
    :param name: name of sympy expression to be evaluated
    :param global_dict: global_substituion dictionary required for exec
    :return:
    """
    local_dict = {}
    # evaluate the potential expression
    exp_str = libspud.get_option(path + '/expression')
    # check if it's a simple expression of form name = expr
    if exp_str.split('=')[0].strip() == name:
        try:
            # ugly hack to just peel off ' name = ' plus white space
            str = exp_str.lstrip(name).strip().lstrip('=')
            expression = sym.sympify(str,locals=global_dict)
        except:
            exec(exp_str, global_dict, local_dict)
            expression = local_dict[name]
    else:
        exec(exp_str, global_dict, local_dict)
        expression = local_dict[name]

    return expression

def set_subs_parameters(params,active):
    """
    creates a dictionary of {sympy symbols: values} from a dictionary
    of parameters excluding all symbol names in the active set
    FIXME:  will need to consider the problem where we only want to
    make some component of a vector parameter active i.e. k[0]

    also returns list of parameter dictionaries for all active_parameters

    input
    ----------
    params: python list of parameter dictionaries
    active: python list of parameter names

    Output
    --------
    subs_dict:  dictionary suitable for sympy substitution of the form
    { A:value }
    where A is a sympy symbol (either scalar or matrix)
    and value is it's values
    active_parameters: python list of active_parameters

    Examples
    --------
    subs_dict, active_parameters = set_subs_parameters(parameters,['H0','S0','V0])
    will set substitution dictionary for all symbols except ['H0','S0','V0]
    and return a list of parameter dictionaries for the active parameters
    """

    #initialize dictionary
    subs_dict = {}
    active_parameters = []

    if isinstance(active, str):
        if active.lower() == 'all':
            active = [ p['name'] for p in params]
        else:
            raise ValueError
    elif active == None:
        # set active to an empty list
        active = []
    for param in params:
        if param['name'] not in active:
            if param['rank'] == 'Scalar':
                subs_dict[param['symbol']] = param['value']
            else:
                subs_dict[param['symbol']] = sym.Matrix(param['value'])
        else:
            active_parameters.append(param)

    return subs_dict, active_parameters


## Generic option writers

def write_parameter(spud_path, p):
    """ writes contents of a parameter dictionary p to a spudfile rooted at the given spudpath
        :param
        spud_path: str:
            libspud options path to beginning of parameter
        p: dict:  with keys name, rank, value, units
    """
    try:
        libspud.add_option(spud_path)
    except libspud.SpudNewKeyWarning as err:
        pass

    if p['rank'] == 'Scalar':
        spud_path += '/rank::Scalar'
        try:
            libspud.add_option(spud_path)
        except libspud.SpudNewKeyWarning as err:
            pass

        # need to make sure types are right
        v = 'value'
        vpath = '{}/{}'.format(spud_path, v)
        try:
            libspud.set_option(vpath, float(p[v]))
        except libspud.SpudNewKeyWarning as err:
            pass

        v = 'units'
        vpath = '{}/{}'.format(spud_path, v)
        try:
            # check for empty strings
            if p[v] == '':
                p[v] = 'None'
            libspud.set_option(vpath, "'{}'".format(p[v]))
        except libspud.SpudNewKeyWarning as err:
            pass
    elif p['rank'] == 'Vector':
        raise NotImplemented

def write_variable(spud_path, v_dict):
    """ writes contents of a variable dictionary  to a spudfile rooted at the given spudpath
        :param
        spud_path: str:
            libspud options path to beginning of parameter
        v_dict: dict:  with keys name, rank, units
    """
    try:
        libspud.add_option(spud_path)
    except libspud.SpudNewKeyWarning as err:
        pass

    if v_dict['rank'] == 'Scalar':
        spath = '{}/rank::Scalar'.format(spud_path)
        try:
            libspud.add_option(spath)
        except libspud.SpudNewKeyWarning as err:
            pass


        v = 'units'
        vpath = '{}/{}'.format(spud_path, v)
        try:
            # check for empty strings
            if v_dict[v] == '':
                v_dict[v] = 'None'
            libspud.set_option(vpath, "'{}'".format(v_dict[v]))
        except libspud.SpudNewKeyWarning as err:
            pass

        if v_dict['symbol'] is not None:
            spath='{}/symbol'.format(spud_path)
            write_str(spath,str(v_dict['symbol']))

    elif v_dict['rank'] == 'Vector':
        spath = '{}/rank::Vector'.format(spud_path)
        try:
            libspud.add_option(spath)
        except libspud.SpudNewKeyWarning as err:
            pass
        try:
            libspud.set_option(spath+'/size',v_dict['size'])
        except libspud.SpudNewKeyWarning:
            pass

        v = 'units'
        vpath = '{}/{}'.format(spud_path, v)
        try:
            ## FIXME: check for empty strings, if units are empty don't set the option...
            ## this messes with the schema a bit for
            if v_dict[v] is not None:
                libspud.set_option(vpath, "'{}'".format(v_dict[v]))
        except libspud.SpudNewKeyWarning as err:
            pass

        if v_dict['symbol'] is not None:
            spath='{}/symbol'.format(spud_path)
            write_str(spath,str(v_dict['symbol']))



def write_expression(spud_path, expression):
    """ writes a sympy expression to a spud file at the specified path"""
    try:
        e_path = '{}/expression'.format(spud_path)
        libspud.set_option(e_path, str(expression))
    except libspud.SpudNewKeyWarning as err:
        pass


def write_str(spud_path, string):
    """ writes a string to a spud file at the specified path"""
    try:
        libspud.set_option(spud_path, string)
    except libspud.SpudNewKeyWarning as err:
        pass

def write_str_list(spud_path, stringlist):
    """ writes a list of strings to a spud file at the specified path"""
    assert isinstance(stringlist,list)
    try:
        libspud.set_option(spud_path,str(stringlist))
    except libspud.SpudNewKeyWarning as err:
        pass


def write_attribute_name(spud_path, string):
    """ writes a string to a spud file at the specified path"""
    try:
        libspud.set_option_attribute(spud_path + '/name', string)
    except libspud.SpudNewKeyWarning as err:
        pass

def write_implicit_function(spud_path, ifunc):
    try:
        libspud.add_option(spud_path)
    except libspud.SpudNewKeyWarning as err:
        pass

    # add Symbol
    path = '{}'.format(spud_path)
    expression = '{} = sym.Function(\'{}\'){}'.format(ifunc['name'],ifunc['name'],str(ifunc['variable'].args))
    write_expression(path,expression)

    # add residual
    name = 'f'
    path = '{}/residual::{}'.format(spud_path,name)
    try:
        libspud.add_option(path)
    except libspud.SpudNewKeyWarning as err:
        pass
    expression = '{} = (\n{}\n)'.format(name,textwrap.fill(str(ifunc['residual'])))
    write_expression(path, expression)

    # add initial_guess
    name = '{}_guess'.format(ifunc['name'])
    path = '{}/initial_guess::{}'.format(spud_path, name)
    try:
        libspud.add_option(path)
    except libspud.SpudNewKeyWarning as err:
        pass
    expression = '{} = {} '.format(name,str(ifunc['guess']))
    write_expression(path, expression)

def write_potential(spud_path, pot):
    try:
        libspud.add_option(spud_path)
    except libspud.SpudNewKeyWarning as err:
        pass

    # add expression
    expression = '{} = (\n{}\n)'.format(pot['name'],textwrap.fill(str(pot['expression'])))
    write_expression(spud_path, expression)
